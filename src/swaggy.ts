import { writeFile } from 'fs-write-data';
import { winstonCfg } from 'winston-cfg';
import * as path from 'path';
const log = winstonCfg();

import {
  JSON2,
  makeContext
} from './utils';
import { SwaggerGen } from './swagger-gen';

export async function swaggy(dst: string, data: any) {

  const resolve = path.resolve;
  const dstDir = {
    api: resolve(`${dst}`)
  };

  // 1. Transform the data so the templates will work without issue.
  const context = makeContext(data);
  log.info(`makeContext OK`);
  log.debug(`context:\n${JSON2(context)}`);

  // 4. render swagger interface
  // await renderDir(srcDir.swagger, dstDir, SwaggerRenderer(context));
  const swagger = SwaggerGen(context);
  const fname = resolve(`${dstDir.api}/swagger.json`);
  await writeFile(fname, swagger);
  log.info(`swagger generation OK`);
  log.info(`Written to ${fname}`);
}