
/**
 * Defines an `AppModel` interface, which is used to declaratively describe a
 * Database backed CRUD application.
 *
 * The AppModel describes tables & relationships, this interface is used to
 * generate a schema that is used to validate the incoming declarative specification.
 *
 */
export interface AppModel {
  name: string | Name;
  description?: string;

  /**
   * List of actions enabled on all Tables.
   *
   * @default AllActions
   * @type {Actions}
   * @memberof AppModel
   */
  actions?: Actions;

  /**
   * Type of data base to use. Defaults to sqlite for now.
   * Recommend switching to mysql for real applications
   *
   * @default sqlite
   * @type {('mysql' | 'sqlite')}
   * @memberof AppModel
   */
  activedb?: 'mysql' | 'sqlite';

  services?: Service[];

  tables: Table[];
  /**
   *
   * @minItems 0
   * @type {[Relation]}
   * @memberof AppModel
   */
  associations?: Association[];
}

export interface Service {
  /**
   * list of actions enabled on the service
   * @default ServiceActions
   * @type {Actions}
   * @memberof Service
   */
  actions?: Actions;

  /**
   * The body of the service request, usually for post.
   * It's a string (typically json stringified, could be text/html or binary
   * that is base64 encoded)
   *
   * @default undefined
   * @type {string}
   * @memberof Service
   */
  body?: string;  // json string, text/html, binary(base64 encoded)

  /**
   * specifies whether cors should be enabled for the service
   *
   * @default true
   * @type {boolean}
   * @memberof Service
   */
  cors?: boolean;

   /**
   * specifies whether credentials should be enabled for the service
   * so that Authorization headers or cookies can be passed
   *
   * @default true
   * @type {boolean}
   * @memberof Service
   */
  credentials?: boolean;

  /**
   * a textual description of the service
   *
   * @type {string}
   * @memberof Service
   */
  description?: string;

  /**
   * the name of the service, this will be passed in raw format without
   * any name conversions
   *
   * @type {string}
   * @memberof Service
   */
  name: string | Name;

  /**
   * list of url path parameters that are specified.
   *
   * @default []
   * @type {[Parameter]}
   * @memberof Service
   */
  pathParams?: Parameter[];

   /**
   * list of url query string parameters that are specified.
   *
   * @default []
   * @type {[Parameter]}
   * @memberof Service
   */
  queryParams?: Parameter[];
}

/**
 * Parameter definition, Service
 *
 * @export
 * @interface Parameter
 */
export interface Parameter {
  /**
   * Parameter name
   *
   * @type {(string | Name)}
   * @memberof Parameter
   */
  name: string | Name;

  /**
   * Short textual description of what the Parameter is for
   *
   * @type {string}
   * @memberof Parameter
   */
  description?: string;

  /**
   * Data type of the parameter is a String, some url parameters such as token
   * can be maximum of 2048 characters (bytes) which requires a different
   * storage type for databases
   * string <= 255  stored as type STRING
   * string >  255  stored as type TEXT
   *
   * @type {string}
   * @memberof Parameter
   */
  type: string;

  /**
   * max length of the parameter string, some url parameters such as token
   * can be maximum of 2048 characters (bytes)
   *
   * @default 255
   * @type {number}
   * @memberof Parameter
   */
  length?: number;  // tokens could be 2048 long
}

/**
 * The store of record, DB-Tables
 *
 * @export
 * @interface Table
 */
export interface Table {
  /**
   * Database table name, coerced into singular form.
   *
   * @type {(string | Name)}
   * @memberof Table
   */
  name: string | Name;
  description?: string;
  /**
   * List of actions enabled on this table.
   * Can over-ride db level settings.
   * By default, is permissive to all allowable actions.
   *
   * @default AllActions
   * @type {Actions}
   * @memberof AppModel
   */
  actions?: Actions;
  fields?: Field[];

}

/**
 * The relationships. More stable than in real-life! For more details,
 *
 * Sequelize defines a uni-directional association.
 * An m:n relationship requires two uni-directional associations.
 * Best summary @ https://stackoverflow.com/questions/22958683/how-to-implement-many-to-many-association-in-sequelize
 *
 * Sequelize documentation
 * see http://docs.sequelizejs.com/manual/tutorial/associations.html
 *
 * @export
 * @interface Association
 */
export interface Association {
  source: string;
  target: string;

  /**
   * hasMany should not be used with m:n relationships
   */
  type:  'hasOne'| 'belongsTo' | 'belongsToMany' | 'hasMany';

  /**
   * Name of the join table. Needs to be specified for
   * m:n relationships.
   *
   * @type {string}
   * @memberof Association
   */
  through?: string;

  /**
   * Alias for association/field name. Overrides default name and foreign-key
   * when specified.
   *
   * @type {string}
   * @memberof Association
   */
  as?: string;
  /**
   * Name of the foreign-key column in the database.
   * Overrides default prop name, overridden by 'as' alias.
   *
   * @type {string}
   * @memberof Association
   */
  foreignKey?: string;
}

export interface Field {
  name: string | Name;
  type: SequelizeDataType;

  /**
   *
   * Allows control over type of input used to display this input
   *
   * @type {InputType}
   * @memberof Field
   */
  inputType?: InputType;

  /**
   * @default 255
   * @type {string}
   * @memberof Field
   */
  length?: string;
  options?: {
    /**
     * @default 255
     * @type {string}
     */
    length: string;
  };

  /**
   * If false, the column will have a NOT NULL constraint, and a
   * not null validation will be run before an instance is saved.
   *
   * @default true
   * @type {boolean}
   * @memberof Field
   */
  allowNull?: boolean;

  /**
   * If true, the column will get a unique constraint. If a string is provided,
   * the column will be part of a composite unique index. If multiple columns
   * have the same string, they will be part of the same unique index
   *
   * @default false
   * @type {(boolean|string)}
   * @memberof Field
   */
  unique?: boolean | string;
  /**
   * A literal default value, a JavaScript function,
   * or an SQL function (see `sequelize.fn`)
   * @default null
   * @type {string}
   * @memberof Field
   */
  defaultValue?: string;

  /**
   *
   * @default false
   * @type {boolean}
   * @memberof Field
   */
  primaryKey?: boolean;
  /**
   * If set, sequelize will map the attribute name to a different name in the database
   * @default string
   * @type {string}
   * @memberof Field
   */
  field?: string;
  /**
   *
   * @default false
   * @memberof Field
   */
  autoIncrement?: boolean;
  /**
   *
   * @default null
   * @type {string}
   * @memberof Field
   */
  comment?: string;

  /**
   * format of value contained.
   *
   * @type {string}
   * @memberof Field
   */
  format?: string;

  /**
   *
   * @type {string}
   * @memberof Field
   */
  description?: string;
}

export interface Name {
  /**
   * camelCase. Used to name objects, properties, apiEndpoints.
   *
   *
   * @type {string}
   * @memberof Name
   */
  propObj: string;
  /**
   * snake_case. Used to name files
   *
   * @type {string}
   * @memberof Name
   */
  fileName: string;

  /**
   * kebab case. Used to name npm modules
   *
   * @type {string}
   * @memberof Name
   */
  moduleName: string;

  /**
   * featherjs name.
   */
  featherjs: string;
  singular: NameTypes;
  plural: NameTypes;
}

export interface NameTypes {
  raw: string;
  kebab: string;
  camel: string;
  plural: string;
  lower: string;
}

export type Action = 'list'
  | 'create'
  | 'corsCreateList'
  | 'read'
  | 'update'
  | 'delete'
  | 'corsGetUpdateDelete';

export type Actions = Action[];

export const AllActions = [
  'list',
  'create',
  'corsCreateList',
  'read',
  'update',
  'delete',
  'corsGetUpdateDelete'
];

export const ServiceActions = [
  'list',
  'corsCreateList'
];

/**
 * The datatypes defined by Sequelize, the underlying ORM used by feathersjs.
 */
export type SequelizeDataType =
  'STRING'
  | 'CHAR'
  | 'TEXT'
  | 'NUMBER'
  | 'INTEGER'
  | 'BIGINT'
  | 'FLOAT'
  | 'REAL'
  | 'DOUBLE PRECISION'
  | 'DECIMAL'
  | 'BOOLEAN'
  | 'TIME'
  | 'DATE'
  | 'DATEONLY'
  | 'HSTORE'
  | 'JSON'
  | 'JSONB'
  | 'NOW'
  | 'BLOB'
  | 'RANGE'
  | 'UUID'
  | 'UUIDV1'
  | 'UUIDV4'
  | 'VIRTUAL'
  | 'ENUM'
  | 'ARRAY'
  | 'GEOMETRY'
  | 'GEOGRAPHY';

export type InputType =
  'AutocompleteInput'
  | 'BooleanInput'
  | 'DateInput'
  | 'FileInput'
  | 'ImageInput'
  | 'LongTextInput'
  | 'NullableBooleanInput'
  | 'NumberInput'
  | 'SelectInput'
  | 'TextInput';


export type FieldType =
  'BooleanField'
  | 'ChipField'
  | 'DateField'
  | 'EmailField'
  | 'FileField'
  | 'FunctionField'
  | 'ImageField'
  | 'NumberField'
  | 'ReferenceField'
  | 'RichTextField'
  | 'SelectField'
  | 'TextField'
  | 'UrlField';
