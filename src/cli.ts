#!/usr/bin/env node

import * as program from 'commander';
import chalk from 'chalk';
import { readFile } from 'fs-read-data';
import { appGen } from './app-gen';
import { swaggy } from './swaggy';
const pkg = require(`${__dirname}/../package.json`);

process.on('unhandledRejection', (reason, p) => {
  console.log(chalk.red(`Unhandled Rejection at: Promise ${p}, reason: ${reason}`));
});

program
  .version(pkg.version)
  .description(`Generates a basic feathers.js backend + admin-on-rest UI`)
  .usage(`[options] <cmd> <dataFile> <destinationDir>`)
  .option(`-v, --verbose [verbose]`, `logging verbosity. One of: error,warn,info,debug,silly`)
  .option(`-f, --force`, `overwrite destination dir. Good for updating a git-repo`, false)
  .parse(process.argv);

const [cmd, dataFile, destinationDir] = program.args;

readFile(dataFile).then(data => {
  if (cmd === 'swaggy') {
    swaggy(destinationDir, data);
  }
  else {
  appGen(destinationDir, data, program.force)
    .catch(err => {
      console.error(chalk.red(err.message));
      console.error(chalk.red(err.stack));
    });
  }
}).catch(err => {
  console.error(chalk.red(err.message));
  console.error(chalk.red(err.stack));
});
