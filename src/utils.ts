

import { FileDescriptor } from 'render-dir';
import * as hb from 'handlebars';
import * as _ from 'lodash';
import * as hb_helpers from 'handlebars-helpers';
import {
  AppModel,
  Name,
  AllActions,
  SequelizeDataType,
  InputType,
  FieldType,
  ServiceActions
} from './app-model';


hb_helpers({ handlebars: hb });

/* istanbul ignore next */
hb.registerHelper('choose', function (a, b) { return a ? a : b; });
hb.registerHelper('stringify', function (obj) {
  /*
  * stringify's an object to a string, with single-quoted strings internally
  */
  const comma = (idx, len) => (idx < len - 1) ? ', ' : '';
  const stringify = o => {
    switch (toString.apply(o)) {
      case '[object Object]': {
        const keys = Object.keys(o);
        const len = keys.length;
        return keys.reduce((str, key, idx, keys) => {
          const val = stringify(o[key]);
          key = key.match('-') ? `'${key}'` : key;
          return str.concat(`${key}: ${val}${comma(idx, keys.length)}`);
        }, ' {').concat('}');
      }
      case '[object Array]': {
        return o.reduce((str, el, idx, arr) => {
          return str.concat(stringify(el) + comma(idx, arr.length));
        }, ' [').concat(']');
      }
      case '[object String]': {
        return `'${o}'`;
      }
      default:
        return o.toString();
    }
  };
  return stringify(obj).trim();
});

export const handlebars = hb;
export const JSON2 = j => JSON.stringify(j, null, 2);

export function hbApply(template, context, fname) {
  try {
    return hb.compile(template)(context);
  } catch (err) {
    // add location information to errors
    /* istanbul ignore next */
    (() => {
      err.message = `Handlebars error: ${fname}: ${err.message}`;
      throw err;
    })();
  }
}

export function ApplyContext(context: any) {
  const filters = [
    // /.*js.map$/   // do not apply templates to .js.map files. JSX uses double curlys which show up here
  ];
  return async (fdesc: FileDescriptor) => {
    if (fdesc.mime === 'utf8') {
      /* istanbul ignore next */
      const skip = filters.reduce(
        (prev, re) => {
          return prev || !!fdesc.path.match(re);
        }, false);
      /* istanbul ignore else */
      if (!skip) {
        fdesc.content = hbApply(fdesc.content, context, fdesc.path);
      } else {
        /* istanbul ignore next */
        console.log(`skipping ${fdesc.path}`);
      }
    }
    fdesc.path = fdesc.path.replace(/\.hbs$/, '');
    fdesc.path = fdesc.path.replace('_gitignore', '.gitignore');
    return fdesc;
  };
}

export const SimpleCopy = async (fdesc: FileDescriptor) => {
  fdesc.path = fdesc.path.replace('_gitignore', '.gitignore');
  return fdesc;
};


export const mkName = (name): Name => {
  const singular = _.singularize(name);
  const plural = _.pluralize(singular);
  return <Name>{
    propObj: _.camelCase(singular),
    fileName: _.snakeCase(singular),
    moduleName: _.kebabCase(singular),
    featherjs: _.kebabCase(singular),
    singular: {
      raw: singular,
      camel: _.camelCase(singular),
      kebab: _.kebabCase(singular),
      lower: _.lowerCase(singular)
    },
    plural: {
      raw: plural,
      camel: _.camelCase(plural),
      kebab: _.kebabCase(plural),
      lower: _.lowerCase(plural)
    }
  };
};


interface TypeMapper {
  type: string;
  inputType?: InputType;
  fieldType?: FieldType;

  format?: string;
}

interface TypeMap {
  [key: string]: TypeMapper;
}

/*
 *
 */
const typeMap = <TypeMap>{
  STRING: {
    type: 'string',
    inputType: 'TextInput',
    fieldType: 'TextField'
  },
  CHAR: {
    type: 'string',
    inputType: 'TextInput',
    fieldType: 'TextField'
  },
  TEXT: {
    type: 'string',
    inputType: 'LongTextInput',
    fieldType: 'TextField'
  },
  INTEGER: {
    type: 'number',
    format: 'integer',
    inputType: 'TextInput',
    fieldType: 'NumberField'
  },
  BIGINT: {
    type: 'number',
    format: 'bigint',
    inputType: 'TextInput',
    fieldType: 'NumberField'
  },
  FLOAT: {
    type: 'number',
    format: 'float',
    inputType: 'TextInput',
    fieldType: 'NumberField'
  },
  REAL: {
    type: 'number',
    format: 'real',
    inputType: 'TextInput',
    fieldType: 'NumberField'
  },
  DECIMAL: {
    type: 'number',
    format: 'decimal',
    inputType: 'TextInput',
    fieldType: 'NumberField'
  },
  BOOLEAN: {
    type: 'boolean',
    inputType: 'BooleanInput',
    fieldType: 'BooleanField'
  },
  DATE: {
    type: 'string',
    format: 'date-time',
    inputType: 'TextInput',
    fieldType: 'DateField'
  },
  DATEONLY: {
    type: 'string',
    format: 'date',
    inputType: 'TextInput',
    fieldType: 'DateField'
  }
};

export function makeContext(data) {
  const context = _.merge({}, data);
  context.name = mkName(context.name);
  context.tables = (context.tables || []).map(t => {
    t.name = mkName(t.name);
    t.actions = t.actions || context.actions || AllActions;
    t.fields = (t.fields || []).map(f => {
      f.name = mkName(f.name);
      // maps Sequelize types to swagger, with prescedence order:
      //   userSpec > swaggerType > undefined
      // const swaggerType = typeMap[f.type];
      // f.type = swaggerType.type;
      // f.inputType = f.inputType || swaggerType.inputType;
      // f.format = f.format || swaggerType.format || undefined;
      return f;
    });
    t.associations = (context.associations || []).reduce((tableAssociations, a) => {
      const sourceName = mkName(a.source);
      const targetName = mkName(a.target);
      // pull options properties
      const { as, through, foreignKey } = a;
      // removes all undefined elements.
      const options = JSON.parse(JSON.stringify({ as, through, foreignKey }));
      if (sourceName.propObj === t.name.propObj) {
        tableAssociations.push({
          source: sourceName.propObj,
          type: a.type,
          target: targetName.propObj,
          options
        });
      }
      return tableAssociations;

    }, []);
    return t;
  });
  context.services = (context.services || []).map(s => {
    s.name = mkName(s.name);
    s.actions = s.actions || context.actions || ServiceActions;
    s.queryParams = (s.queryParams || []).map(q => {
      q.name = mkName(q.name);
      return q;
    });
    s.pathParams = (s.pathParams || []).map(p => {
      p.name = mkName(p.name);
      return p;
    });
    s.body = s.body || null;
    s.cors = s.cors || true;
    s.credentials = s.credentials || false;
    return s;
  });

  return context;
}

export const SequelizeToJsonType = (type) => {
  switch (type) {
    case 'INTEGER': return 'number';
    default: return type.toLowerCase();
  }
};

export const getUiType = (type) => {
  const { inputType, fieldType } = typeMap[type];
  return { inputType, fieldType };
};
