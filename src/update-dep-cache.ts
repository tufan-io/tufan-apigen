#!/usr/bin/env node
/**
 * `updateCache` encapsulates all dependencies that tufan-apigen uses
 * to allow the various pieces to work.
 *
 * Most of these dependencies use require a bit of customization or custom
 * wrappers to be usable in the context of tufan-apigen.
 *
 * `updateCache` copies all artifacts from underlying dependecies to a local
 * cache which is committed to local version control. It also performs any
 * inline transformation/customization necessary.
 *
 * A simple version control satus check, after the update completes is enough
 * to track "significant" updates. Since all such updates require human
 * intervention to resolve, we just fail the build-task or test that invoked
 * this function.
 *
 */

import * as path from 'path';
import * as a from 'awaiting';
import * as pkgDir from 'pkg-dir';
import { renderDir, FileDescriptor } from 'render-dir';

export const copyTemplate = async () => {
  const rootDir = await pkgDir(__dirname);
  {
    // generator-feathers/app
    const srcDir = 'node_modules/generator-feathers/generators/app';
    const dstDir = 'templates/__cache/generator-feathers/app';
    const copies = await renderDir(srcDir, dstDir);
  }
  {
    // generator-feathers/service
    const srcDir = 'node_modules/generator-feathers/generators/service';
    const dstDir = 'templates/__cache/generator-feathers/service';
    const copies = await renderDir(srcDir, dstDir);
  }
  {
    // swagger-ui-dist files
    const srcDir = 'node_modules/swagger-ui-dist';
    const dstDir = 'templates/__cache/swagger-ui';
    const customize = async (fdesc: FileDescriptor) => {
      const skips = [
        '.npmignore',
        'package.json',
        '.DS_Store'
      ];
      return (-1 === skips.indexOf(fdesc.path)) ? fdesc : null;
    };
    const copies = await renderDir(srcDir, dstDir, customize);
  }
  {
    // redoc
    const srcDir = 'node_modules/redoc/dist';
    const dstDir = 'templates/__cache/redoc';
    const customize = async (fdesc: FileDescriptor) => {
      return fdesc;
    };
    const copies = await renderDir(srcDir, dstDir, customize);
  }
};

/* istanbul ignore if */
if (require.main === module) {
  const chalk = require('chalk');
  const execa = require('execa');
  const cp = require('child_process');

  // const JSON2 = j => JSON.stringify(j, null, 2);
  copyTemplate()
    .then(async () => {
      const result = await execa(`git`, [`status`, `-s`, `templates/__cache`]);
      if (result.stdout) {
        throw new Error(`Template dependencies have an update. Time to dial-a-human!\n${result.stdout}`);
      }
    })
    .catch(err => {
      console.error(chalk.red(err.message));
      process.exit(-1);
    });
}
