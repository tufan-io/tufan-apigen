import { isValid } from 'data-check';
import * as pkgDir from 'pkg-dir';
import { writeFile } from 'fs-write-data';
import { readFile } from 'fs-read-data';
import * as a from 'awaiting';
import * as path from 'path';
import * as fs from 'fs';
import * as assert from 'assert';
import * as _ from 'lodash';
import * as inflection from 'lodash-inflection';
import { winstonCfg } from 'winston-cfg';
const log = winstonCfg();

const refer = require('json-schema-ref-parser');

import {
  JSON2,
  makeContext,
  ApplyContext,
  SimpleCopy,
  hbApply
} from './utils';
import { renderDir, FileDescriptor } from 'render-dir';
import { SwaggerGen } from './swagger-gen';
import { UiContext } from './simple-ui-gen';

_.mixin(inflection);


function TableRenderer(context: any, table: any) {
  // generator-feathers generates kebab-case files.
  // for now, we'll stick to their convention, since it'll allow
  // the generated repo to be managed via generator-feathers.
  const kebab = table.name.featherjs;
  const pathMap = {
    'src/models/tufan-table.model.js': `src/models/${kebab}.model.js`,
    'src/services/tufan-table/tufan-table.filters.js': `src/services/${kebab}/${kebab}.filters.js`,
    'src/services/tufan-table/tufan-table.hooks.js': `src/services/${kebab}/${kebab}.hooks.js`,
    'src/services/tufan-table/tufan-table.service.js': `src/services/${kebab}/${kebab}.service.js`,
    'test/services/tufan-table.test.js': `test/services/${kebab}.test.js`
  };
  return async (fdesc: FileDescriptor) => {
    log.silly(`context: ${JSON2(table)}`);
    log.debug(fdesc.path);
    log.silly(`template: ${fdesc.content}`);
    fdesc.content = hbApply(fdesc.content, table, fdesc.path);
    fdesc.path = fdesc.path.replace(/\.hbs$/, '');
    fdesc.path = pathMap[fdesc.path];
    return fdesc;
  };
}

function SimpleUiResourceRenderer(context: any, table: any) {
  const kebab = table.name.singular.kebab;
  return async (fdesc: FileDescriptor) => {
    log.silly(`context: ${JSON2(table)}`);
    log.debug(fdesc.path);
    log.silly(`template: ${fdesc.content}`);
    fdesc.content = hbApply(fdesc.content, table, fdesc.path);
    fdesc.path = `${kebab}.js`;
    log.info(`writing resource to ${fdesc.path}`);
    return fdesc;
  };
}

export async function appGen(dst: string, data: any, forceUpdate = false) {
  const root = await pkgDir(__dirname);
  const resolve = path.resolve;
  const srcDir = {
    lerna: resolve(`${root}/templates/lerna`),
    app: resolve(`${root}/templates/app`),
    service: resolve(`${root}/templates/service`),
    swagger: resolve(`${root}/templates/swagger-ui`),
    redoc: resolve(`${root}/templates/redoc`),
    simpleUi: resolve(`${root}/templates/simple-ui`),
    simpleUiResource: resolve(`${root}/templates/simple-ui-resource`)
  };
  const dstDir = {
    base: resolve(`${dst}`),
    api: resolve(`${dst}/packages/api-server`),
    swagger: resolve(`${dst}/packages/swagger`),
    redoc: resolve(`${dst}/packages/redoc`),
    simpleUi: resolve(`${dst}/packages/simple-ui`),
    simpleUiResource: resolve(`${dst}/packages/simple-ui/src/resources`)
  };

  // pre-check - destination dir should be empty
  if (!forceUpdate) {
    assert.equal(0,
      (await a.callback(fs.readdir, dst)).length,
      `Destination directory is not empty. Aborting`
    );
  }

  // 0. validate the input data.
  const schema = require(`${root}/schema.json`);
  try {
    const result = await isValid(schema, data);
  } catch (err) {
    const violations = err.errors.map(v => {
      delete v.parentSchema;
      return v;
    });
    throw new Error(`${err.message}\n${JSON.stringify(violations, null, 2)}`);
  }

  // 1. Transform the data so the templates will work without issue.
  const context = makeContext(data);
  log.info(`makeContext OK`);
  log.debug(`context:\n${JSON2(context)}`);

  // 2. Copy lerna module at ${dst}
  await renderDir(srcDir.lerna, dstDir.base, ApplyContext(context));
  log.info(`lerna skeleton rendered OK`);

  // 2. render api-skeleton, including wiring for all table use.
  //    the tables themselves are created in step #2.
  await renderDir(srcDir.app, dstDir.api, ApplyContext(context));
  log.info(`featherjs API skeleton rendered OK`);

  // 3. render db-tables that'll support the api.
  for (let table of context.tables) {
    await renderDir(srcDir.service, dstDir.api, TableRenderer(context, table));
  }
  log.info(`featherjs relations rendered OK`);

  // 4. render swagger interface
  // await renderDir(srcDir.swagger, dstDir, SwaggerRenderer(context));
  const swagger = SwaggerGen(context);
  const fname = resolve(`${dstDir.api}/swagger.json`);
  await writeFile(fname, swagger);
  log.info(`swagger generation OK`);
  log.info(`Written to ${fname}`);

  const res = await readFile(fname);
  // this is a fully dereferenced swagger definition for swagger ui/redoc to work
  const fullSwagger = await refer.dereference(res);
  log.info(`swagger dereference OK`);

  // 5. render the clients - swagger-ui
  await renderDir(srcDir.swagger, dstDir.swagger, SimpleCopy);
  await writeFile(resolve(`${dstDir.swagger}/swagger.json`), fullSwagger);
  log.info(`swagger-ui render OK`);

  // 6. redoc
  await renderDir(srcDir.redoc, dstDir.redoc, SimpleCopy);
  await writeFile(resolve(`${dstDir.redoc}/swagger.json`), fullSwagger);
  log.info(`redoc render OK`);

  const uiContext = UiContext(context);
  log.debug(`uiContext:\n${JSON2(uiContext)}`);
  // 7. simple-ui
  await renderDir(
    srcDir.simpleUi,
    dstDir.simpleUi,
    ApplyContext(uiContext));
  log.info(`simple-ui render OK`);

  // 8. simple-ui-resources
  for (let table of uiContext.tables) {
    await renderDir(
      srcDir.simpleUiResource,
      dstDir.simpleUiResource,
      SimpleUiResourceRenderer(uiContext, table));
  }
  log.info(`simple-ui resource render OK`);

}
