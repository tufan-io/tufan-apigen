
import * as _ from 'lodash';
import { AppModel } from './app-model';
import {
  SequelizeToJsonType,
  getUiType
} from './utils';

/**
 * Converts the field.types from Sequelize data types,
 * to something JSON/JavaScript/TypeScript worthy.
 * This is a total hacky way of getting this accomplished.
 * Will have to leave that for later unfortunately.
 *
 * @param ctx
 */
export const UiContext = (ctx: AppModel): any => {
  const c = _.merge({}, ctx);
  c.tables = c.tables.map(t => {
    t.fields = t.fields.map(f => {
      const _f = _.merge({}, f);
      const { inputType, fieldType } = getUiType(f.type);
      _f.type = SequelizeToJsonType(f.type);
      _f['inputType'] = inputType;
      _f['fieldType'] = fieldType;
      return _f;
    });
    return t;
  });
  return c;
};
