
import * as execa from 'execa';
import { test } from 'ava';
import { appGen } from '../..';
import { tablePaths } from '../../swagger-gen';
import { servicePaths } from '../../swagger-gen';
import { mkName } from '../../utils';
import { readFile } from 'fs-read-data';
import { isValid } from 'data-check';
import { JSON2, makeContext, handlebars } from '../../utils';

import * as tmp from 'tmp';
import * as a from 'awaiting';

import * as appModels from '../fixtures/app-models';
import * as invalidModel from '../fixtures/invalid-model-err-message';
import { copyTemplate } from '../../update-dep-cache';

const swaggerSchema = require('../../../node_modules/swagger-schema-official/package.json');

test('appGen - todoList', async (t) => {
  const dstdir = (await a.callback(
    tmp.dir, {
      keep: true,
      prefix: 'tufan-apigen-'
    }));
  console.log(dstdir);
  await appGen(dstdir, appModels.todoList);
  // in addition to generation, we'll test that the generated schema is
  // actually valid - per the published swagger schema.
  const generatedDefn = await readFile(`${dstdir}/packages/api-server/swagger.json`);
  try {
    isValid(swaggerSchema, generatedDefn);
  } catch (err) {
    t.fail(`Generated swagger file is invalid: ${err.message} ${JSON.stringify(err.errors, null, 2)}`);
  }

  // We'll try to generator the application a second time
  // Without forcedUpdate, which should fail
  try {
    await appGen(dstdir, appModels.todoList);
  } catch (err) {
    t.is(err.message, 'Destination directory is not empty. Aborting');
  }

  try {
    const forceUpdate = true;
    await appGen(dstdir, appModels.todoList, forceUpdate);
  } catch (err) {
    t.fail(`forceUpdate failed to overwrite - ${err.message}`);
  }
  console.log(dstdir);
  t.pass();
});

test(`appGen - unknown action`, async (t) => {
  const tables = [{
    name: mkName('test resource'),
    actions: ['someRandomAction']
  }];
  try {
    tablePaths(tables);
  } catch (err) {
    t.is(err.message, `Unknown action testResource:someRandomAction`, err.message);
  }
});

test(`appGen - unknown service action`, async (t) => {
  const services = [{
    name: mkName('test resource'),
    actions: ['someRandomAction']
  }];
  try {
    servicePaths(services);
  } catch (err) {
    t.is(err.message, `Unknown action test resource:someRandomAction`, err.message);
  }
});

test(`makeContext - no tables`, async (t) => {
  const data = {
    name: 'no tables'
  };
  const expected = {
    'name': {
      'fileName': 'no_table',
      'propObj': 'noTable',
      'moduleName': 'no-table',
      'featherjs': 'no-table',
      'singular': {
        'raw': 'no table',
        'camel': 'noTable',
        'kebab': 'no-table',
        'lower': 'no table'
      },
      'plural': {
        'raw': 'no tables',
        'camel': 'noTables',
        'kebab': 'no-tables',
        'lower': 'no tables'
      }
    },
    'tables': [],
    'services': []
  };
  const actual = makeContext(data);
  t.deepEqual(actual, expected, JSON2(actual));
});

test(`makeContext - table with no fields`, async (t) => {
  const data = {
    name: 'no tables',
    tables: [{
      name: 'table with no fields'
    }]
  };
  const expected = {
    'name': {
      'fileName': 'no_table',
      'propObj': 'noTable',
      'moduleName': 'no-table',
      'featherjs': 'no-table',
      'singular': {
        'raw': 'no table',
        'camel': 'noTable',
        'kebab': 'no-table',
        'lower': 'no table'
      },
      'plural': {
        'raw': 'no tables',
        'camel': 'noTables',
        'kebab': 'no-tables',
        'lower': 'no tables'
      }
    },
    'tables': [
      {
        'name': {
          'fileName': 'table_with_no_field',
          'propObj': 'tableWithNoField',
          'moduleName': 'table-with-no-field',
          'featherjs': 'table-with-no-field',
          'singular': {
            'raw': 'table with no field',
            'camel': 'tableWithNoField',
            'kebab': 'table-with-no-field',
            'lower': 'table with no field'
          },
          'plural': {
            'raw': 'table with no fields',
            'camel': 'tableWithNoFields',
            'kebab': 'table-with-no-fields',
            'lower': 'table with no fields'
          }
        },
        'actions': [
          'list',
          'create',
          'corsCreateList',
          'read',
          'update',
          'delete',
          'corsGetUpdateDelete'
        ],
        'fields': [],
        'associations': []
      }
    ],
    'services': []
  };
  const actual = makeContext(data);
  t.deepEqual(actual, expected, JSON2(actual));
});

test(`test template dependency updates`, async (t) => {
  await copyTemplate();
  const result = await execa(
    `git`,
    [`status`, `-s`, `templates/__cache`]
  );
  t.is(
    !!result.stdout,
    false,
    [
      `Template dependencies have an update.`,
      `Time to dial-a-human!\n${result.stdout}`
    ].join(' ')
  );
});

test(`handlebars helper stringify`, t => {
  const context = {
    o: {
      'a-dashed-key': 'any-val',
      arr0: [],
      arr1: [1],
      arr2: [1, 2],
      bool: true
    }
  };
  const template = '{{{stringify o}}}';
  const expected = `{'a-dashed-key': 'any-val', arr0:  [], arr1:  [1], arr2:  [1, 2], bool: true}`;
  const actual = handlebars.compile(template)(context);
  t.is(actual, expected, actual);
});

test(`invalid app-model`, async t => {
  const dstdir = await a.callback(
    tmp.dir, {
      keep: true,
      prefix: 'tufan-apigen-invalid-model'
    });
  try {
    await appGen(dstdir, appModels.invalidModel);
    t.fail(`appGen seems to have succeeded with an invalid appModel ${dstdir}`);
  } catch (err) {
    t.is(err.message, invalidModel.expectedError, err.message);
  }
});
