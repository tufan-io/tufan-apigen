export const expectedError = `Invalid data
[
  {
    "keyword": "additionalProperties",
    "dataPath": "",
    "schemaPath": "#/additionalProperties",
    "params": {
      "additionalProperty": "description2"
    },
    "message": "should NOT have additional properties",
    "schema": false,
    "data": {
      "name": "Todo List",
      "description2": "a simple Todo list",
      "activedb": "sqlite",
      "tables": [
        {
          "name": "User",
          "fields": [
            {
              "name": "firstName",
              "type": "STRING"
            },
            {
              "name": "lastName",
              "type": "STRING"
            }
          ]
        }
      ],
      "associations": []
    }
  }
]`;
