import {
  AppModel,
  Table,
  Field,
  Association
} from '../../../app-model';

export const todoList = <AppModel>{
  name: 'Todo List',
  description: 'a simple Todo list',
  activedb: 'sqlite',
  tables: [{
    name: 'User',
    fields: [{
      name: 'firstName',
      type: 'STRING'
    }, {
      name: 'lastName',
      type: 'STRING'
    }]
  }, {
    name: 'Task List',
    fields: [{
      name: 'title',
      type: 'STRING',
    }]
  }, {
    name: 'Todo',
    fields: [{
      name: 'list_number',
      comment: 'item order number',
      type: 'INTEGER',
    }, {
      name: 'text',
      type: 'STRING',
    }, {
      name: 'completed',
      type: 'BOOLEAN',
    }]
  }],
  associations: [{
    source: 'User',
    type: 'belongsToMany',
    target: 'Task List',
    through: 'user_tasks'
  }, {
    source: ' Task List',
    type: 'belongsToMany',
    target: 'User',
    through: 'user_tasks'
  }, {
    source: 'Todo',
    type: 'belongsTo',
    target: 'Task List'
  }],
  services: [{
    name: 'amazon',
    queryParams: [{
      name: 'callback',
      type: 'string'
    }, {
      name: 'redirect',
      type: 'string'
    }],
    cors: true,
    credentials: false,
    actions: [
      'list',
      'corsCreateList'
    ]
  }, {
    name: 'amazon/callback',
    queryParams: [],
    actions: ['list'],
    credentials: true
  }, {
    name: 'user/id',
    pathParams: [{
      name: 'id',
      type: 'string'
    }],
  }]
};
