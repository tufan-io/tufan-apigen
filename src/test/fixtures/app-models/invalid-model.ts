// import {
//   AppModel,
//   Table,
//   Field,
//   Association
// } from '../../../app-model';

export const invalidModel = {
  name: 'Todo List',
  description2: 'a simple Todo list',
  activedb: 'sqlite',
  tables: [{
    name: 'User',
    fields: [{
      name: 'firstName',
      type: 'STRING'
    }, {
      name: 'lastName',
      type: 'STRING'
    }]
  }],
  associations: []
};
