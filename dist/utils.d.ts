import { FileDescriptor } from 'render-dir';
import { Name, InputType, FieldType } from './app-model';
export declare const handlebars: any;
export declare const JSON2: (j: any) => string;
export declare function hbApply(template: any, context: any, fname: any): any;
export declare function ApplyContext(context: any): (fdesc: FileDescriptor) => Promise<FileDescriptor>;
export declare const SimpleCopy: (fdesc: FileDescriptor) => Promise<FileDescriptor>;
export declare const mkName: (name: any) => Name;
export declare function makeContext(data: any): any;
export declare const SequelizeToJsonType: (type: any) => any;
export declare const getUiType: (type: any) => {
    inputType: InputType;
    fieldType: FieldType;
};
