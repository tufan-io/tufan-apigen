"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_write_data_1 = require("fs-write-data");
const winston_cfg_1 = require("winston-cfg");
const path = require("path");
const log = winston_cfg_1.winstonCfg();
const utils_1 = require("./utils");
const swagger_gen_1 = require("./swagger-gen");
function swaggy(dst, data) {
    return __awaiter(this, void 0, void 0, function* () {
        const resolve = path.resolve;
        const dstDir = {
            api: resolve(`${dst}`)
        };
        const context = utils_1.makeContext(data);
        log.info(`makeContext OK`);
        log.debug(`context:\n${utils_1.JSON2(context)}`);
        const swagger = swagger_gen_1.SwaggerGen(context);
        const fname = resolve(`${dstDir.api}/swagger.json`);
        yield fs_write_data_1.writeFile(fname, swagger);
        log.info(`swagger generation OK`);
        log.info(`Written to ${fname}`);
    });
}
exports.swaggy = swaggy;
