"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllActions = [
    'list',
    'create',
    'corsCreateList',
    'read',
    'update',
    'delete',
    'corsGetUpdateDelete'
];
exports.ServiceActions = [
    'list',
    'corsCreateList'
];
