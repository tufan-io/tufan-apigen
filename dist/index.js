"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const engchk = require("runtime-engine-check");
engchk();
var app_gen_1 = require("./app-gen");
exports.appGen = app_gen_1.appGen;
__export(require("./app-model"));
