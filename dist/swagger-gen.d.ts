import { AppModel } from './app-model';
export declare const SwaggerGen: (app: AppModel) => {
    swagger: string;
    info: {
        title: string;
        version: string;
        description: string;
        termsOfService: string;
        contact: {
            name: string;
            url: string;
            email: string;
        };
        license: {
            name: string;
            url: string;
        };
    };
    schemes: string[];
    consumes: string[];
    produces: string[];
    definitions: {};
    paths: {};
};
export declare const paths: (tables: any, services?: {}) => any;
export declare const tablePaths: (tables: any) => any;
export declare const servicePaths: (services: any) => any;
