export interface AppModel {
    name: string | Name;
    description?: string;
    actions?: Actions;
    activedb?: 'mysql' | 'sqlite';
    services?: Service[];
    tables: Table[];
    associations?: Association[];
}
export interface Service {
    actions?: Actions;
    body?: string;
    cors?: boolean;
    credentials?: boolean;
    description?: string;
    name: string | Name;
    pathParams?: Parameter[];
    queryParams?: Parameter[];
}
export interface Parameter {
    name: string | Name;
    description?: string;
    type: string;
    length?: number;
}
export interface Table {
    name: string | Name;
    description?: string;
    actions?: Actions;
    fields?: Field[];
}
export interface Association {
    source: string;
    target: string;
    type: 'hasOne' | 'belongsTo' | 'belongsToMany' | 'hasMany';
    through?: string;
    as?: string;
    foreignKey?: string;
}
export interface Field {
    name: string | Name;
    type: SequelizeDataType;
    inputType?: InputType;
    length?: string;
    options?: {
        length: string;
    };
    allowNull?: boolean;
    unique?: boolean | string;
    defaultValue?: string;
    primaryKey?: boolean;
    field?: string;
    autoIncrement?: boolean;
    comment?: string;
    format?: string;
    description?: string;
}
export interface Name {
    propObj: string;
    fileName: string;
    moduleName: string;
    featherjs: string;
    singular: NameTypes;
    plural: NameTypes;
}
export interface NameTypes {
    raw: string;
    kebab: string;
    camel: string;
    plural: string;
    lower: string;
}
export declare type Action = 'list' | 'create' | 'corsCreateList' | 'read' | 'update' | 'delete' | 'corsGetUpdateDelete';
export declare type Actions = Action[];
export declare const AllActions: string[];
export declare const ServiceActions: string[];
export declare type SequelizeDataType = 'STRING' | 'CHAR' | 'TEXT' | 'NUMBER' | 'INTEGER' | 'BIGINT' | 'FLOAT' | 'REAL' | 'DOUBLE PRECISION' | 'DECIMAL' | 'BOOLEAN' | 'TIME' | 'DATE' | 'DATEONLY' | 'HSTORE' | 'JSON' | 'JSONB' | 'NOW' | 'BLOB' | 'RANGE' | 'UUID' | 'UUIDV1' | 'UUIDV4' | 'VIRTUAL' | 'ENUM' | 'ARRAY' | 'GEOMETRY' | 'GEOGRAPHY';
export declare type InputType = 'AutocompleteInput' | 'BooleanInput' | 'DateInput' | 'FileInput' | 'ImageInput' | 'LongTextInput' | 'NullableBooleanInput' | 'NumberInput' | 'SelectInput' | 'TextInput';
export declare type FieldType = 'BooleanField' | 'ChipField' | 'DateField' | 'EmailField' | 'FileField' | 'FunctionField' | 'ImageField' | 'NumberField' | 'ReferenceField' | 'RichTextField' | 'SelectField' | 'TextField' | 'UrlField';
