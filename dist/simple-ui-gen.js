"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const utils_1 = require("./utils");
exports.UiContext = (ctx) => {
    const c = _.merge({}, ctx);
    c.tables = c.tables.map(t => {
        t.fields = t.fields.map(f => {
            const _f = _.merge({}, f);
            const { inputType, fieldType } = utils_1.getUiType(f.type);
            _f.type = utils_1.SequelizeToJsonType(f.type);
            _f['inputType'] = inputType;
            _f['fieldType'] = fieldType;
            return _f;
        });
        return t;
    });
    return c;
};
