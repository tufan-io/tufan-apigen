#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const program = require("commander");
const chalk_1 = require("chalk");
const fs_read_data_1 = require("fs-read-data");
const app_gen_1 = require("./app-gen");
const swaggy_1 = require("./swaggy");
const pkg = require(`${__dirname}/../package.json`);
process.on('unhandledRejection', (reason, p) => {
    console.log(chalk_1.default.red(`Unhandled Rejection at: Promise ${p}, reason: ${reason}`));
});
program
    .version(pkg.version)
    .description(`Generates a basic feathers.js backend + admin-on-rest UI`)
    .usage(`[options] <cmd> <dataFile> <destinationDir>`)
    .option(`-v, --verbose [verbose]`, `logging verbosity. One of: error,warn,info,debug,silly`)
    .option(`-f, --force`, `overwrite destination dir. Good for updating a git-repo`, false)
    .parse(process.argv);
const [cmd, dataFile, destinationDir] = program.args;
fs_read_data_1.readFile(dataFile).then(data => {
    if (cmd === 'swaggy') {
        swaggy_1.swaggy(destinationDir, data);
    }
    else {
        app_gen_1.appGen(destinationDir, data, program.force)
            .catch(err => {
            console.error(chalk_1.default.red(err.message));
            console.error(chalk_1.default.red(err.stack));
        });
    }
}).catch(err => {
    console.error(chalk_1.default.red(err.message));
    console.error(chalk_1.default.red(err.stack));
});
