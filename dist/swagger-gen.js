"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("./utils");
const merge = require("lodash.merge");
exports.SwaggerGen = (app) => {
    const name = app.name.singular.raw;
    let swagger = {
        swagger: '2.0',
        info: {
            title: `'${name}', REST API v1`,
            version: '1.0.0',
            description: `'${name}', REST API v1`,
            termsOfService: '',
            contact: {
                name: 'tufan.io',
                url: 'http://tufan.io',
                email: 'contact@tufan.io'
            },
            license: {
                name: 'ISC',
                url: 'http://tufan.io'
            }
        },
        schemes: [
            'http',
            'https'
        ],
        consumes: ['application/json'],
        produces: ['application/json'],
        definitions: {},
        paths: {}
    };
    swagger.definitions = definitions(app.tables);
    swagger.paths = exports.paths(app.tables, app.services);
    return swagger;
};
const definitions = (tables) => {
    return tables.reduce((defns, t) => {
        const name = t.name.propObj;
        const plural = t.name.plural.camel;
        defns[name] = {
            type: `object`,
            properties: fields(t.fields)
        };
        defns[plural] = {
            type: `array`,
            items: {
                $ref: `#/definitions/${[name]}`
            }
        };
        return defns;
    }, {});
};
const fields = (_fields) => {
    return _fields.reduce((defns, f) => {
        const { name } = f, rest = __rest(f, ["name"]);
        const _name = name.propObj;
        defns[_name] = Object.keys(rest).reduce((acc, key) => {
            switch (key) {
                case 'type': {
                    acc.type = utils_1.SequelizeToJsonType(rest.type);
                    break;
                }
                default:
                    acc[key] = rest[key];
            }
            return acc;
        }, {});
        return defns;
    }, {});
};
exports.paths = (tables, services = {}) => {
    return merge({}, exports.tablePaths(tables), exports.servicePaths(services));
};
exports.tablePaths = (tables) => {
    return tables.reduce((_paths, t) => {
        const name = t.name.propObj;
        _paths[`/${name}`] = {};
        _paths[`/${name}/{id}`] = {};
        t.actions.map(a => {
            switch (a) {
                case 'create':
                    _paths[`/${name}`].post = api_C(t, name);
                    break;
                case 'list':
                    _paths[`/${name}`].get = api_L(t, name);
                    break;
                case 'read':
                    _paths[`/${name}/{id}`].get = api_R(t, name);
                    break;
                case 'update':
                    _paths[`/${name}/{id}`].put = api_U(t, name);
                    break;
                case 'delete':
                    _paths[`/${name}/{id}`].delete = api_D(t, name);
                    break;
                case 'corsCreateList':
                    _paths[`/${name}`].options = apiCors_CL(t, name, false);
                    break;
                case 'corsGetUpdateDelete':
                    _paths[`/${name}/{id}`].options = apiCors_RUD(t, name, false);
                    break;
                default:
                    throw new Error(`Unknown action ${name}:${a}`);
            }
        });
        return _paths;
    }, {});
};
exports.servicePaths = (services) => {
    return services.reduce((_paths, s) => {
        const name = s.name.singular.raw;
        _paths[`/${name}`] = {};
        s.actions.map(a => {
            switch (a) {
                case 'list':
                    _paths[`/${name}`].get = service_L(s, name);
                    break;
                case 'corsCreateList':
                    _paths[`/${name}`].options = apiCors_CL(s, name, s.credentials);
                    break;
                default:
                    throw new Error(`Unknown action ${name}:${a}`);
            }
        });
        return _paths;
    }, {});
};
const parameters = (service) => {
    let __params = [];
    ['queryParams', 'pathParams'].reduce((_params, pt) => {
        service[pt].reduce((_p, p) => {
            const name = p.name.propObj;
            _p.push({
                name: name,
                in: pt === 'queryParams' ? 'query' : 'path',
                description: p.description || name,
                required: p.required || true,
                type: p.type
            });
            return _p;
        }, _params);
        return _params;
    }, __params);
    return __params;
};
const requestTemplate_LRD = () => {
    return `{
    #if($context.stage)
      #set($newpath = $context.path.replace("/$context.stage", ""))
      "path": "$newpath",
    #else
      "path": "$context.path",
    #end
    "httpMethod": "$context.httpMethod",
    #set($keys=[])
    #foreach($key in $input.params().path.keySet())
      #set($success = $keys.add($key))
    #end
    #foreach($key in $input.params().querystring.keySet())
      #set($success = $keys.add($key))
    #end
    "queryStringParameters": {
      #foreach($key in $keys)
        "$key": "$util.escapeJavaScript($input.params($key))"#if($foreach.hasNext),#end
      #end
    },
    "headers": {
      #foreach($header in $input.params().header.keySet())
        "$header": "$util.escapeJavaScript($input.params().header.get($header))"
        #if($foreach.hasNext),#end
      #end
    }
  }`;
};
const requestTemplate_CU = () => {
    return `{
    "body": "$util.escapeJavaScript($input.json('$'))",
    #if($context.stage)
      #set($newpath = $context.path.replace("/$context.stage", ""))
      "path": "$newpath",
    #else
      "path": "$context.path",
    #end
    "httpMethod": "$context.httpMethod",
    #set($keys=[])
    #foreach($key in $input.params().path.keySet())
      #set($success = $keys.add($key))
    #end
    #foreach($key in $input.params().querystring.keySet())
      #set($success = $keys.add($key))
    #end
    "queryStringParameters": {
      #foreach($key in $keys)
        "$key": "$util.escapeJavaScript($input.params($key))"#if($foreach.hasNext),#end
      #end
    },
    "headers": {
      #foreach($header in $input.params().header.keySet())
        "$header": "$util.escapeJavaScript($input.params().header.get($header))"
        #if($foreach.hasNext),#end
      #end
    }
  }`;
};
const api_C = (table, name) => {
    return {
        summary: `Create a new ${name}`,
        consumes: [`application/json`],
        operationId: `${name}.create`,
        parameters: [{
                in: `body`,
                required: true,
                name: `${name}`,
                schema: {
                    $ref: `#/definitions/${name}`
                }
            }],
        'x-amazon-apigateway-integration': {
            type: `aws`,
            uri: `{{apiEndpointUri}}`,
            httpMethod: `POST`,
            requestTemplates: {
                'application/json': requestTemplate_CU()
            },
            responses: {
                default: {
                    statusCode: `200`,
                    responseParameters: {
                        'method.response.header.Access-Control-Allow-Origin': `'*'`
                    },
                    responseTemplates: {
                        'application/json': `$input.path('$.body')`
                    }
                },
                'BAD.*': {
                    statusCode: `400`
                },
                'INT.*': {
                    statusCode: `500`
                }
            }
        },
        responses: {
            200: {
                description: `Created`,
                schema: {
                    $ref: `#/definitions/${name}`
                },
                headers: {
                    'Access-Control-Allow-Origin': {
                        type: `string`
                    }
                }
            },
            400: {
                description: `Bad Request.`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `Detected a malformed request to ${name}.create`
                        }
                    }
                }
            },
            401: {
                description: `Authorization exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `You are not authorized to ${name}.create`
                        }
                    }
                }
            },
            405: {
                description: `Validation exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `We had trouble with ${name}.create. Please report the problem if this continues`
                        }
                    }
                }
            }
        }
    };
};
const api_L = (table, name, search = 'search', sortBy = 'sortBy') => {
    return {
        summary: `Get a (paged/sorted) list of ${name}`,
        produces: [`application/json`],
        operationId: `${name}.list`,
        parameters: [{
                name: `${search}`,
                in: `query`,
                description: `query string`,
                required: false,
                type: `string`
            }, {
                name: `${sortBy}`,
                in: `query`,
                description: `sort parameter(s). ${sortBy}=-email,name `,
                required: false,
                type: `string`
            }, {
                name: `page`,
                in: `query`,
                description: `Page number`,
                required: false,
                type: `number`
            }, {
                name: `offset`,
                in: `query`,
                description: `Result offset`,
                required: false,
                type: `number`
            }, {
                name: `count`,
                in: `query`,
                description: `Count of results returned per request. Defaults to 100, limit 1000`,
                required: false,
                type: `number`
            }],
        'x-amazon-apigateway-integration': {
            type: `aws`,
            uri: `{{apiEndpointUri}}`,
            httpMethod: `POST`,
            requestTemplates: {
                'application/json': requestTemplate_LRD()
            },
            responses: {
                default: {
                    statusCode: `200`,
                    responseParameters: {
                        'method.response.header.Access-Control-Allow-Origin': `'*'`
                    },
                    responseTemplates: {
                        'application/json': `$input.path('$.body')`
                    }
                },
                'BAD.*': {
                    statusCode: `400`
                },
                'INT.*': {
                    statusCode: `500`
                }
            }
        },
        responses: {
            200: {
                description: `${name}`,
                schema: {
                    $ref: `#/definitions/${name}`
                },
                headers: {
                    'Access-Control-Allow-Origin': {
                        type: `string`
                    }
                }
            },
            401: {
                description: `Authorization exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `You are not authorized to access ${name}.list`
                        }
                    }
                }
            },
            405: {
                description: `Validation exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `We had trouble retrieving ${name}.list. Please report the problem if this continues`
                        }
                    }
                }
            }
        }
    };
};
const api_R = (table, name) => {
    return {
        summary: `Get an existing ${name}`,
        produces: ['application/json'],
        operationId: `${name}.read`,
        parameters: [{
                name: `id`,
                in: `path`,
                description: `object id`,
                required: true,
                type: `string`
            }],
        'x-amazon-apigateway-integration': {
            type: `aws`,
            uri: `{{apiEndpointUri}}`,
            httpMethod: `POST`,
            requestTemplates: {
                'application/json': requestTemplate_LRD()
            },
            responses: {
                default: {
                    statusCode: `200`,
                    responseParameters: {
                        'method.response.header.Access-Control-Allow-Origin': `'*'`
                    },
                    responseTemplates: {
                        'application/json': `$input.path('$.body')`
                    }
                },
                '.*[NotFound].*': {
                    statusCode: `404`,
                    responseParameters: {
                        'method.response.header.Access-Control-Allow-Origin': `'*'`
                    },
                    responseTemplates: {
                        'application/json': `$input.path('$.errorMessage')`
                    }
                },
                'BAD.*': {
                    statusCode: `400`
                },
                'INT.*': {
                    statusCode: `500`
                }
            }
        },
        responses: {
            200: {
                description: `${name}`,
                schema: {
                    $ref: `#/definitions/${name}`
                },
                headers: {
                    'Access-Control-Allow-Origin': {
                        type: `string`
                    }
                }
            },
            401: {
                description: `Authorization exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `You are not authorized to ${name}.read`
                        }
                    }
                }
            },
            404: {
                description: `Not Found`,
                headers: {
                    'Access-Control-Allow-Origin': {
                        type: `string`
                    }
                },
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `Requested resource was not found`
                        }
                    }
                }
            },
            405: {
                description: `Validation exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `We had trouble reading ${name}.read. Please report the problem if this continues`
                        }
                    }
                }
            }
        }
    };
};
const api_U = (table, name) => {
    return {
        summary: `Update an existing ${name}`,
        consumes: ['application/json'],
        operationId: `${name}.update`,
        parameters: [{
                name: `id`,
                in: `path`,
                description: `object id`,
                required: true,
                type: `string`
            }, {
                in: `body`,
                required: true,
                name: `${name}`,
                schema: {
                    $ref: `#/definitions/${name}`
                }
            }],
        'x-amazon-apigateway-integration': {
            type: `aws`,
            uri: `{{apiEndpointUri}}`,
            httpMethod: `POST`,
            requestTemplates: {
                'application/json': requestTemplate_CU()
            },
            responses: {
                default: {
                    statusCode: `200`,
                    responseParameters: {
                        'method.response.header.Access-Control-Allow-Origin': `'*'`
                    },
                    responseTemplates: {
                        'application/json': `$input.path('$.body')`
                    }
                },
                'BAD.*': {
                    statusCode: `400`
                },
                'INT.*': {
                    statusCode: `500`
                }
            }
        },
        responses: {
            200: {
                description: `Updated`,
                schema: {
                    $ref: `#/definitions/${name}`
                },
                headers: {
                    'Access-Control-Allow-Origin': {
                        type: `string`
                    }
                }
            },
            400: {
                description: `Bad Request.`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `Detected a malformed request to ${name}.update`
                        }
                    }
                }
            },
            401: {
                description: `Authorization exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `You are not authorized to access ${name}.update`
                        }
                    }
                }
            },
            405: {
                description: `Validation exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `We had trouble retrieving ${name}.update. Please report the problem if this continues`
                        }
                    }
                }
            }
        }
    };
};
const api_D = (table, name) => {
    return {
        summary: `Delete an existing ${name}`,
        consumes: ['application/json'],
        operationId: `${name}.delete`,
        parameters: [{
                name: `id`,
                in: `path`,
                description: `object id`,
                required: true,
                type: `string`
            }],
        'x-amazon-apigateway-integration': {
            type: `aws`,
            uri: `{{apiEndpointUri}}`,
            httpMethod: `POST`,
            requestTemplates: {
                'application/json': requestTemplate_LRD()
            },
            responses: {
                default: {
                    statusCode: `200`,
                    responseParameters: {
                        'method.response.header.Access-Control-Allow-Origin': `'*'`
                    },
                    responseTemplates: {
                        'application/json': `$input.path('$.body')`
                    }
                },
                'BAD.*': {
                    statusCode: `400`
                },
                'INT.*': {
                    statusCode: `500`
                }
            }
        },
        responses: {
            200: {
                description: `delete successful, no content in response`,
                headers: {
                    'Access-Control-Allow-Origin': {
                        type: `string`
                    }
                }
            },
            400: {
                description: `Bad Request.`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `Detected a malformed attempt to ${name}.delete`
                        }
                    }
                }
            },
            401: {
                description: `Authorization exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `You are not authorized to perform ${name}.delete`
                        }
                    }
                }
            },
            405: {
                description: `Validation exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `We had trouble performing ${name}.delete. Please report the problem if this continues`
                        }
                    }
                }
            }
        }
    };
};
const apiCors_CL = (table, name, credentials) => {
    const corsOriginList = credentials ? `'{{corsOriginList}}'` : `'*'`;
    return {
        consumes: [
            `application/json`
        ],
        responses: {
            200: {
                description: `200 response`,
                headers: {
                    'Access-Control-Allow-Origin': {
                        type: `string`
                    },
                    'Access-Control-Allow-Methods': {
                        type: `string`
                    },
                    'Access-Control-Allow-Headers': {
                        type: `string`
                    }
                }
            }
        },
        'x-amazon-apigateway-integration': {
            responses: {
                default: {
                    statusCode: `200`,
                    responseParameters: {
                        'method.response.header.Access-Control-Allow-Methods': `'POST,GET,OPTIONS'`,
                        'method.response.header.Access-Control-Allow-Headers': `'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'`,
                        'method.response.header.Access-Control-Allow-Origin': corsOriginList
                    }
                }
            },
            requestTemplates: {
                'application/json': `{"statusCode": 200}`
            },
            passthroughBehavior: `when_no_match`,
            type: `mock`
        }
    };
};
const apiCors_RUD = (table, name, credentials) => {
    const corsOriginList = credentials ? `'{{corsOriginList}}'` : `'*'`;
    return {
        consumes: [
            `application/json`
        ],
        parameters: [
            {
                name: `id`,
                in: `path`,
                required: true,
                type: `string`
            }
        ],
        responses: {
            200: {
                description: `200 response`,
                headers: {
                    'Access-Control-Allow-Origin': {
                        type: `string`
                    },
                    'Access-Control-Allow-Methods': {
                        type: `string`
                    },
                    'Access-Control-Allow-Headers': {
                        type: `string`
                    }
                }
            }
        },
        'x-amazon-apigateway-integration': {
            responses: {
                default: {
                    statusCode: `200`,
                    responseParameters: {
                        'method.response.header.Access-Control-Allow-Methods': `'DELETE,GET,OPTIONS,PUT'`,
                        'method.response.header.Access-Control-Allow-Headers': `'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'`,
                        'method.response.header.Access-Control-Allow-Origin': corsOriginList
                    }
                }
            },
            requestTemplates: {
                'application/json': `{"statusCode": 200}`
            },
            passthroughBehavior: `when_no_match`,
            type: `mock`
        }
    };
};
const service_L = (service, name) => {
    const corsOriginList = service.credentials ? `'{{corsOriginList}}'` : `'*'`;
    return {
        summary: `Get an existing ${name}`,
        produces: ['application/json'],
        operationId: `${name}.read`,
        parameters: parameters(service),
        'x-amazon-apigateway-integration': {
            type: `aws`,
            uri: `{{apiEndpointUri}}`,
            httpMethod: `POST`,
            requestTemplates: {
                'application/json': requestTemplate_LRD()
            },
            responses: {
                default: {
                    statusCode: `200`,
                    responseParameters: {
                        'method.response.header.Access-Control-Allow-Origin': corsOriginList
                    },
                    responseTemplates: {
                        'application/json': `$input.path('$.body')`
                    }
                },
                'BAD.*': {
                    statusCode: `400`
                },
                'INT.*': {
                    statusCode: `500`
                }
            }
        },
        responses: {
            200: {
                description: `${name}`,
                headers: {
                    'Access-Control-Allow-Origin': {
                        type: `string`
                    }
                }
            },
            401: {
                description: `Authorization exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `You are not authorized to ${name}.read`
                        }
                    }
                }
            },
            405: {
                description: `Validation exception`,
                schema: {
                    properties: {
                        error: {
                            type: `string`,
                            default: `We had trouble reading ${name}.read. Please report the problem if this continues`
                        }
                    }
                }
            }
        }
    };
};
