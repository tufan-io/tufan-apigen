"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const hb = require("handlebars");
const _ = require("lodash");
const hb_helpers = require("handlebars-helpers");
const app_model_1 = require("./app-model");
hb_helpers({ handlebars: hb });
hb.registerHelper('choose', function (a, b) { return a ? a : b; });
hb.registerHelper('stringify', function (obj) {
    const comma = (idx, len) => (idx < len - 1) ? ', ' : '';
    const stringify = o => {
        switch (toString.apply(o)) {
            case '[object Object]': {
                const keys = Object.keys(o);
                const len = keys.length;
                return keys.reduce((str, key, idx, keys) => {
                    const val = stringify(o[key]);
                    key = key.match('-') ? `'${key}'` : key;
                    return str.concat(`${key}: ${val}${comma(idx, keys.length)}`);
                }, ' {').concat('}');
            }
            case '[object Array]': {
                return o.reduce((str, el, idx, arr) => {
                    return str.concat(stringify(el) + comma(idx, arr.length));
                }, ' [').concat(']');
            }
            case '[object String]': {
                return `'${o}'`;
            }
            default:
                return o.toString();
        }
    };
    return stringify(obj).trim();
});
exports.handlebars = hb;
exports.JSON2 = j => JSON.stringify(j, null, 2);
function hbApply(template, context, fname) {
    try {
        return hb.compile(template)(context);
    }
    catch (err) {
        (() => {
            err.message = `Handlebars error: ${fname}: ${err.message}`;
            throw err;
        })();
    }
}
exports.hbApply = hbApply;
function ApplyContext(context) {
    const filters = [];
    return (fdesc) => __awaiter(this, void 0, void 0, function* () {
        if (fdesc.mime === 'utf8') {
            const skip = filters.reduce((prev, re) => {
                return prev || !!fdesc.path.match(re);
            }, false);
            if (!skip) {
                fdesc.content = hbApply(fdesc.content, context, fdesc.path);
            }
            else {
                console.log(`skipping ${fdesc.path}`);
            }
        }
        fdesc.path = fdesc.path.replace(/\.hbs$/, '');
        fdesc.path = fdesc.path.replace('_gitignore', '.gitignore');
        return fdesc;
    });
}
exports.ApplyContext = ApplyContext;
exports.SimpleCopy = (fdesc) => __awaiter(this, void 0, void 0, function* () {
    fdesc.path = fdesc.path.replace('_gitignore', '.gitignore');
    return fdesc;
});
exports.mkName = (name) => {
    const singular = _.singularize(name);
    const plural = _.pluralize(singular);
    return {
        propObj: _.camelCase(singular),
        fileName: _.snakeCase(singular),
        moduleName: _.kebabCase(singular),
        featherjs: _.kebabCase(singular),
        singular: {
            raw: singular,
            camel: _.camelCase(singular),
            kebab: _.kebabCase(singular),
            lower: _.lowerCase(singular)
        },
        plural: {
            raw: plural,
            camel: _.camelCase(plural),
            kebab: _.kebabCase(plural),
            lower: _.lowerCase(plural)
        }
    };
};
const typeMap = {
    STRING: {
        type: 'string',
        inputType: 'TextInput',
        fieldType: 'TextField'
    },
    CHAR: {
        type: 'string',
        inputType: 'TextInput',
        fieldType: 'TextField'
    },
    TEXT: {
        type: 'string',
        inputType: 'LongTextInput',
        fieldType: 'TextField'
    },
    INTEGER: {
        type: 'number',
        format: 'integer',
        inputType: 'TextInput',
        fieldType: 'NumberField'
    },
    BIGINT: {
        type: 'number',
        format: 'bigint',
        inputType: 'TextInput',
        fieldType: 'NumberField'
    },
    FLOAT: {
        type: 'number',
        format: 'float',
        inputType: 'TextInput',
        fieldType: 'NumberField'
    },
    REAL: {
        type: 'number',
        format: 'real',
        inputType: 'TextInput',
        fieldType: 'NumberField'
    },
    DECIMAL: {
        type: 'number',
        format: 'decimal',
        inputType: 'TextInput',
        fieldType: 'NumberField'
    },
    BOOLEAN: {
        type: 'boolean',
        inputType: 'BooleanInput',
        fieldType: 'BooleanField'
    },
    DATE: {
        type: 'string',
        format: 'date-time',
        inputType: 'TextInput',
        fieldType: 'DateField'
    },
    DATEONLY: {
        type: 'string',
        format: 'date',
        inputType: 'TextInput',
        fieldType: 'DateField'
    }
};
function makeContext(data) {
    const context = _.merge({}, data);
    context.name = exports.mkName(context.name);
    context.tables = (context.tables || []).map(t => {
        t.name = exports.mkName(t.name);
        t.actions = t.actions || context.actions || app_model_1.AllActions;
        t.fields = (t.fields || []).map(f => {
            f.name = exports.mkName(f.name);
            return f;
        });
        t.associations = (context.associations || []).reduce((tableAssociations, a) => {
            const sourceName = exports.mkName(a.source);
            const targetName = exports.mkName(a.target);
            const { as, through, foreignKey } = a;
            const options = JSON.parse(JSON.stringify({ as, through, foreignKey }));
            if (sourceName.propObj === t.name.propObj) {
                tableAssociations.push({
                    source: sourceName.propObj,
                    type: a.type,
                    target: targetName.propObj,
                    options
                });
            }
            return tableAssociations;
        }, []);
        return t;
    });
    context.services = (context.services || []).map(s => {
        s.name = exports.mkName(s.name);
        s.actions = s.actions || context.actions || app_model_1.ServiceActions;
        s.queryParams = (s.queryParams || []).map(q => {
            q.name = exports.mkName(q.name);
            return q;
        });
        s.pathParams = (s.pathParams || []).map(p => {
            p.name = exports.mkName(p.name);
            return p;
        });
        s.body = s.body || null;
        s.cors = s.cors || true;
        s.credentials = s.credentials || false;
        return s;
    });
    return context;
}
exports.makeContext = makeContext;
exports.SequelizeToJsonType = (type) => {
    switch (type) {
        case 'INTEGER': return 'number';
        default: return type.toLowerCase();
    }
};
exports.getUiType = (type) => {
    const { inputType, fieldType } = typeMap[type];
    return { inputType, fieldType };
};
