#!/usr/bin/env node
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const pkgDir = require("pkg-dir");
const render_dir_1 = require("render-dir");
exports.copyTemplate = () => __awaiter(this, void 0, void 0, function* () {
    const rootDir = yield pkgDir(__dirname);
    {
        const srcDir = 'node_modules/generator-feathers/generators/app';
        const dstDir = 'templates/__cache/generator-feathers/app';
        const copies = yield render_dir_1.renderDir(srcDir, dstDir);
    }
    {
        const srcDir = 'node_modules/generator-feathers/generators/service';
        const dstDir = 'templates/__cache/generator-feathers/service';
        const copies = yield render_dir_1.renderDir(srcDir, dstDir);
    }
    {
        const srcDir = 'node_modules/swagger-ui-dist';
        const dstDir = 'templates/__cache/swagger-ui';
        const customize = (fdesc) => __awaiter(this, void 0, void 0, function* () {
            const skips = [
                '.npmignore',
                'package.json',
                '.DS_Store'
            ];
            return (-1 === skips.indexOf(fdesc.path)) ? fdesc : null;
        });
        const copies = yield render_dir_1.renderDir(srcDir, dstDir, customize);
    }
    {
        const srcDir = 'node_modules/redoc/dist';
        const dstDir = 'templates/__cache/redoc';
        const customize = (fdesc) => __awaiter(this, void 0, void 0, function* () {
            return fdesc;
        });
        const copies = yield render_dir_1.renderDir(srcDir, dstDir, customize);
    }
});
if (require.main === module) {
    const chalk = require('chalk');
    const execa = require('execa');
    const cp = require('child_process');
    exports.copyTemplate()
        .then(() => __awaiter(this, void 0, void 0, function* () {
        const result = yield execa(`git`, [`status`, `-s`, `templates/__cache`]);
        if (result.stdout) {
            throw new Error(`Template dependencies have an update. Time to dial-a-human!\n${result.stdout}`);
        }
    }))
        .catch(err => {
        console.error(chalk.red(err.message));
        process.exit(-1);
    });
}
