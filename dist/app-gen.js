"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const data_check_1 = require("data-check");
const pkgDir = require("pkg-dir");
const fs_write_data_1 = require("fs-write-data");
const fs_read_data_1 = require("fs-read-data");
const a = require("awaiting");
const path = require("path");
const fs = require("fs");
const assert = require("assert");
const _ = require("lodash");
const inflection = require("lodash-inflection");
const winston_cfg_1 = require("winston-cfg");
const log = winston_cfg_1.winstonCfg();
const refer = require('json-schema-ref-parser');
const utils_1 = require("./utils");
const render_dir_1 = require("render-dir");
const swagger_gen_1 = require("./swagger-gen");
const simple_ui_gen_1 = require("./simple-ui-gen");
_.mixin(inflection);
function TableRenderer(context, table) {
    const kebab = table.name.featherjs;
    const pathMap = {
        'src/models/tufan-table.model.js': `src/models/${kebab}.model.js`,
        'src/services/tufan-table/tufan-table.filters.js': `src/services/${kebab}/${kebab}.filters.js`,
        'src/services/tufan-table/tufan-table.hooks.js': `src/services/${kebab}/${kebab}.hooks.js`,
        'src/services/tufan-table/tufan-table.service.js': `src/services/${kebab}/${kebab}.service.js`,
        'test/services/tufan-table.test.js': `test/services/${kebab}.test.js`
    };
    return (fdesc) => __awaiter(this, void 0, void 0, function* () {
        log.silly(`context: ${utils_1.JSON2(table)}`);
        log.debug(fdesc.path);
        log.silly(`template: ${fdesc.content}`);
        fdesc.content = utils_1.hbApply(fdesc.content, table, fdesc.path);
        fdesc.path = fdesc.path.replace(/\.hbs$/, '');
        fdesc.path = pathMap[fdesc.path];
        return fdesc;
    });
}
function SimpleUiResourceRenderer(context, table) {
    const kebab = table.name.singular.kebab;
    return (fdesc) => __awaiter(this, void 0, void 0, function* () {
        log.silly(`context: ${utils_1.JSON2(table)}`);
        log.debug(fdesc.path);
        log.silly(`template: ${fdesc.content}`);
        fdesc.content = utils_1.hbApply(fdesc.content, table, fdesc.path);
        fdesc.path = `${kebab}.js`;
        log.info(`writing resource to ${fdesc.path}`);
        return fdesc;
    });
}
function appGen(dst, data, forceUpdate = false) {
    return __awaiter(this, void 0, void 0, function* () {
        const root = yield pkgDir(__dirname);
        const resolve = path.resolve;
        const srcDir = {
            lerna: resolve(`${root}/templates/lerna`),
            app: resolve(`${root}/templates/app`),
            service: resolve(`${root}/templates/service`),
            swagger: resolve(`${root}/templates/swagger-ui`),
            redoc: resolve(`${root}/templates/redoc`),
            simpleUi: resolve(`${root}/templates/simple-ui`),
            simpleUiResource: resolve(`${root}/templates/simple-ui-resource`)
        };
        const dstDir = {
            base: resolve(`${dst}`),
            api: resolve(`${dst}/packages/api-server`),
            swagger: resolve(`${dst}/packages/swagger`),
            redoc: resolve(`${dst}/packages/redoc`),
            simpleUi: resolve(`${dst}/packages/simple-ui`),
            simpleUiResource: resolve(`${dst}/packages/simple-ui/src/resources`)
        };
        if (!forceUpdate) {
            assert.equal(0, (yield a.callback(fs.readdir, dst)).length, `Destination directory is not empty. Aborting`);
        }
        const schema = require(`${root}/schema.json`);
        try {
            const result = yield data_check_1.isValid(schema, data);
        }
        catch (err) {
            const violations = err.errors.map(v => {
                delete v.parentSchema;
                return v;
            });
            throw new Error(`${err.message}\n${JSON.stringify(violations, null, 2)}`);
        }
        const context = utils_1.makeContext(data);
        log.info(`makeContext OK`);
        log.debug(`context:\n${utils_1.JSON2(context)}`);
        yield render_dir_1.renderDir(srcDir.lerna, dstDir.base, utils_1.ApplyContext(context));
        log.info(`lerna skeleton rendered OK`);
        yield render_dir_1.renderDir(srcDir.app, dstDir.api, utils_1.ApplyContext(context));
        log.info(`featherjs API skeleton rendered OK`);
        for (let table of context.tables) {
            yield render_dir_1.renderDir(srcDir.service, dstDir.api, TableRenderer(context, table));
        }
        log.info(`featherjs relations rendered OK`);
        const swagger = swagger_gen_1.SwaggerGen(context);
        const fname = resolve(`${dstDir.api}/swagger.json`);
        yield fs_write_data_1.writeFile(fname, swagger);
        log.info(`swagger generation OK`);
        log.info(`Written to ${fname}`);
        const res = yield fs_read_data_1.readFile(fname);
        const fullSwagger = yield refer.dereference(res);
        log.info(`swagger dereference OK`);
        yield render_dir_1.renderDir(srcDir.swagger, dstDir.swagger, utils_1.SimpleCopy);
        yield fs_write_data_1.writeFile(resolve(`${dstDir.swagger}/swagger.json`), fullSwagger);
        log.info(`swagger-ui render OK`);
        yield render_dir_1.renderDir(srcDir.redoc, dstDir.redoc, utils_1.SimpleCopy);
        yield fs_write_data_1.writeFile(resolve(`${dstDir.redoc}/swagger.json`), fullSwagger);
        log.info(`redoc render OK`);
        const uiContext = simple_ui_gen_1.UiContext(context);
        log.debug(`uiContext:\n${utils_1.JSON2(uiContext)}`);
        yield render_dir_1.renderDir(srcDir.simpleUi, dstDir.simpleUi, utils_1.ApplyContext(uiContext));
        log.info(`simple-ui render OK`);
        for (let table of uiContext.tables) {
            yield render_dir_1.renderDir(srcDir.simpleUiResource, dstDir.simpleUiResource, SimpleUiResourceRenderer(uiContext, table));
        }
        log.info(`simple-ui resource render OK`);
    });
}
exports.appGen = appGen;
