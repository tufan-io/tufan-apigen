# tufan-apigen

<!-- badge -->
[![npm license](https://img.shields.io/npm/l/tufan-apigen.svg)](https://www.npmjs.com/package/tufan-apigen)
[![travis status](https://img.shields.io/travis/tufan-io/tufan-apigen.svg)](https://travis-ci.org/tufan-io/tufan-apigen)
[![Build status](https://ci.appveyor.com/api/projects/status/90am2usst4qeutgi?svg=true)](https://ci.appveyor.com/project/tufan-io/tufan-apigen)
[![Coverage Status](https://coveralls.io/repos/github/tufan-io/tufan-apigen/badge.svg?branch=master)](https://coveralls.io/github/tufan-io/tufan-apigen?branch=master)
[![David](https://david-dm.org/tufan-io/tufan-apigen/status.svg)](https://david-dm.org/tufan-io/tufan-apigen)
[![David](https://david-dm.org/tufan-io/tufan-apigen/dev-status.svg)](https://david-dm.org/tufan-io/tufan-apigen?type=dev)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

[![NPM](https://nodei.co/npm/tufan-apigen.png?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/tufan-apigen/)
<!-- endbadge -->

## Usage

As a production user of `tufan-apigen`, this is what you need:

```bash
npm install https://bitbucket.org/tufan-io/tufan-apigen -g

mkdir my-app
```

edit my-app/my-app-model.{json, yaml} to conform to the [AppModel interface](./src/app-model.ts).
For an example, see the [Todo List App](./src/test/fixtures/app-models/todo-list.ts).

```bash
apigen -h
```

## Development

```bash
npm install
npm run build:dist

```

### Test-run a sample application

One of the tests, uses `apigen` to generate a simple but complete todo application.
It also prints out the name of the tmp directory used to generate the application,
making it easy to test-run an application.

The source [todo-list app-model](./src/test/fixtures/app-models/todo-list.ts)
describes the schema used as input. It's really a JSON object, we use a .ts file
so the TypeScript compiler copies it to build directories. Sorry, sometimes the
lazy bones stick out :anguished:.

```bash
$ npm run build:dist
...
> tufan-apigen@1.0.0 test-with-cover /Users/sramam/trial/tufan-apigen
> nyc -r html -r text-summary ava --verbose
...
info: makeContext OK
info: lerna skeleton rendered OK
tufan-apigen
  APP todo-list
info: featherjs API skeleton rendered OK
  > TABLE user
    + FIELD firstName STRING
    + FIELD lastName STRING
  <> user :belongsToMany: taskList
  > TABLE taskList
    + FIELD title STRING
  <> taskList :belongsToMany: user
  > TABLE todo
    + FIELD listNumber INTEGER
    + FIELD text STRING
    + FIELD completed BOOLEAN
  <> todo :belongsTo: taskList
info: featherjs relations rendered OK
info: swagger generation OK
info: Written to /var/folders/3l/_rh6kwjx3rv7p0hy3fqj0ksm0000gn/T/tufan-apigen-22896X3Kp2cN0bAnC/packages/api-server/swagger.json
info: swagger dereference OK
info: swagger-ui render OK
info: redoc render OK
info: simple-ui render OK
info: writing resource to user.js
info: writing resource to task-list.js
info: writing resource to todo.js
info: simple-ui resource render OK
/var/folders/3l/_rh6kwjx3rv7p0hy3fqj0ksm0000gn/T/tufan-apigen-22896X3Kp2cN0bAnC
  ✔ appGen - todoList (3.6s)
  ✔ test template dependency updates (3.6s)

  6 tests passed

```

Switch context to fully rendered application.

```bash
cd /var/folders/3l/_rh6kwjx3rv7p0hy3fqj0ksm0000gn/T/tufan-apigen-22896X3Kp2cN0bAnC
```

### Run Local

Install all dependencies & start the api-server and ui-server in dev mode.
The ui-server is meant to serve static content eventually.

```bash
npm install
npm run start
```

`npm run start` starts both servers in parallel & ui-server takes control of
the console. Some times, it's necessary to have access to both - for debugging
and/or just for fun. Open two terminal windows and do one of the following in each.

1. `npm run api-server:start`
2. `npm run simple-ui:start`

### Deploy to AWS

Install all dependencies, build, package and deploy

```bash
npm install
npm run build
npm run dist
npm run deploy
```
`npm run build` builds the simple-ui using react scripts and creates an optimized
bundle. `npm run dist` creates the distribution directory with all the necessary
artifacts from the api-server, simple-ui, swagger and redoc directories. `npm run
deploy` uses serverless framework to provision and deploy the artifacts onto AWS.

The generated application package consists of:
* api-server - a feathersjs REST API backend is deployed to AWS Lambda
* simple-ui - a react based front end is deployed to AWS S3 and configured as a
static website.
* swagger - swagger documentation for the REST API is deployed to the same AWS S3
bucket and accessible as /swagger from the main url.
* redoc - redoc documentation for the REST API is deployed to the same AWS S3
bucket and accessible as /redoc from the main url.

Once deployed, the website URL is echoed on the console. Copy and paste to
browser to start using the app.

To cleanup once you are done
```bash
npm run remove
```

## Code Structure

```bash
├── README.md
├── package-lock.json
├── package.json
└── packages
    ├── api-server   # Backend [feathers.js based server](https://feathersjs.com/)
    ├── redoc        # [api documentation](https://rebilly.github.io/ReDoc/)
    ├── simple-ui    # Frontend [react based admin-on-rest]
    ├                #   (https://marmelab.com/admin-on-rest/)
    └── swagger      # [swagger-ui](https://github.com/swagger-api/swagger-ui)
```

`tufan-apigen` gives you a quick starting point, please refer to these services
for details on how to customize them. We have tried hard to make it a batteries
included generation process from `tufan-apigen`.

By using mature and popular tools developed in the community, we give you the
ability to quickly get started, with great documentation and an ability to
build upon well-tested frameworks.

In reality, `tufan-apigen` is also framework agnostic - though integrating other
frameworks is a question of building the integration. Please let us know if you
are interested in adding support for other frameworks - support@tufan.io.

To the extent possible, we make minimal modifications to the generators that exist
within individual tools. You should be able to continue to use these generators.
Do let us know if you see problems.

Round tripping back to the source of the generators is not currently supported,
though for a subset of frameworks, it might be a possibility. TBD.

## RESTful conventions

There are no concrete recommendations on resource naming conventions for
RESTful APIs. So much so, that a microsoft API-guidelines explicitly
side-steps the issue - https://github.com/Microsoft/api-guidelines/issues/67 !

Since `tufan-apigen`'s primary purpose is to generate APIs, it makes sense to
define a convention that we expect to follow.

A primary goal of RESTful APIs is to make it easy to consume and create data.
JSON is the de-facto standard for data consumption - especially on clients.

Hyphenated property access is more cumbersome in JSON/JavaScript.
`obj.CamelCaseProp => obj['hyphenated-prop']`

One necessarily wants to optimize for such client consumption.

However, file-systems are sometimes case-insensitive. In which case,
`CamelCaseProp.json` is the same as `camelcaseprop.json` or similar variants.

To that end, tufan-apigen adopts this convention

1. `files_names` are `snake_case`,
2. `objectAndPropertNames` are `camelCase`

One other piece to be resolved is whether API end-points should be singular or plural.
We choose singular. Primarily this allow us to use the resourceName in the UI naively

## Changelog

`tufan-apigen` is a mono-repo, without dependencies, and therefore, does not
at this point come bundled with lerna or yarn-workspaces. It is however structured
to be lerna/yarn-workspace compatible, should your application grow to need it.

[Changelog](./CHANGELOG.md)

## License

[Apache-2.0](./LICENSE.md)

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](code-of-conduct.md). By participating in this project you agree to abide by its terms.

## Support

Bugs, PRs, comments, suggestions welcomed!
