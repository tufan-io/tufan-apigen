import './vendor';
import { SpecManager } from './utils/spec-manager';
import { Redoc } from './components/index';
import { DropDown } from './shared/components/index';
import { LazyTasksService } from './shared/components/LazyFor/lazy-for';
import { OptionsService, Options, MenuService, ScrollService, Hash, WarningsService, AppStateService, ComponentParser, ContentProjector, Marker, SchemaHelper, SearchService, MenuItem } from './services/';
export declare class RedocModule {
}
export { Redoc, SpecManager, ScrollService, Hash, WarningsService, OptionsService, Options, AppStateService, ComponentParser, ContentProjector, MenuService, SearchService, SchemaHelper, LazyTasksService, MenuItem, Marker, DropDown };
