import { ElementRef } from '@angular/core';
export declare class SelectOnClick {
    private element;
    $element: any;
    constructor(element: ElementRef);
    onClick(): void;
}
