import feathers from 'feathers';
import rest from 'feathers-rest';
import hooks from 'feathers-hooks';
import fetch from 'isomorphic-fetch';

function getConfig() {
  try {
    const loc = window.location;
    const appconfig = `${loc.protocol}//${loc.host}/appconfig.json`;
    return fetch(appconfig)
      .then((res) => res.json())
      .then((data) => {
        // console.log('data - ' + JSON.stringify(data));
        return data;
      });
  } catch (err) {
    console.log(err);
  }
}

export function getBaseUrl() {
  return getConfig()
    .then(function (data) {
      const stage = data.stage;
      const url = data.apiUrl[stage];
      return url;
    })
    .catch((err) => console.log(err));
}

export function client(simple = true, url) {
  return feathers()
    .configure(hooks())
    .configure(rest(url).fetch(window.fetch.bind(window)));
}
