/**
 * Generates resource files for the serverless framework to use
 * swagger.yml -> binds the api endpoint uri's to swagger.json
 * permission.yml -> permission resource for api-gateway to invoke lambda fn.
 */
const fs = require('fs');
const hb = require('handlebars');
const YAML = require('yamljs');

// input: swagger template
const swaggerTemplate = '../swagger.json';
// input: root folder for the website
const wsServerlessFile = '../../../../mu-ui/deploy/serverless.yml';
// input: root folder for the lambda function
const fnServerlessFile = '../deploy/serverless.yml';

// output: CF resource -> apigw permission to invoke lambda function
const permissionFilename = 'permission-resource.yml';
// output: CF resource -> create rest API from swagger
const apigwFilename = 'apigw-resource.yml';

// read the serverless files as objects
const soWebsite = YAML.load(wsServerlessFile);
const soFunction = YAML.load(fnServerlessFile);

/**
 * Constructs the url for the website
 *
 * @param {object} so - serverless object where the website s3 bucket is defined
 */
const constructWebsiteUrl = (so) => {
  const bucketName = so.custom.client.bucketName;
  const region = so.provider.region;

  // <bucket>.s3-<region>.amazonaws.com
  // <bucket>.s3.amazonaws.com (for us-east-1)
  // http://mu-ui-dev.s3-website-us-west-2.amazonaws.com/authcallback?code=25a0edb311626c463647e31fc7f333aab57f92bb&redirect=/
  const domain = (region === 'us-east-1')
                   ? `s3.amazonaws.com`
                   : `s3-website-${region}.amazonaws.com`;
  const wsUrl = `http://${bucketName}.${domain}`;
  return wsUrl;
}


/**
 * Constructs the uri for the λ function
 *
 * @param {object} so - serverless object where the λ function is defined
 * @param {string} accountNo - aws account number where λ is being deployed
 */
const constructLambdaUri = (so, accountNo) => {
  const service = so.service;
  const stage = so.provider.stage;;
  const region = so.provider.region;;
  const userFnName = Object.keys(soFunction.functions)[0];
  const lambdaFnName = `${service}-${stage}-${userFnName}`;
  const uri = `arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/`
            + `arn:aws:lambda:${region}:${accountNo}:function:${lambdaFnName}/invocations`;

  // add the computed values back to so.
  so.userFnName = userFnName;
  so.lambdaFnName = lambdaFnName;
  return uri;
}

/**
 * Match the serverless framework generated name for apigateway rest api
 *
 * @param {string} fnName - λ function name
 */
const constructApigwNameForServerless = (fnName) => `dev-${fnName}-server`;


// match the serverless framework generated names for lambda function and
// permission resources

/**
 * Constructs the serverless name used by the serverless framework for defining
 * resources
 *
 * @param {string} fn - λ function name
 */
const constructLambdaNameForServerless = (fn) => {
  let s = fn.replace(/-/g, 'Dash');
  let r = s[0].toUpperCase() + s.slice(1);
  return r;
}

/**
 * transforms the input json file which describes the swagger definition
 * in handlebar template form to an api gateway resource definition as a
 * cloud formation template
 *
 * @param {string} templateFile input json filename containing swagger defn template
 * @param {object} bindings key value pair specifying the bindings for the template
 * @param {string} outputFile output filename for api gateway cloud formation template
 * @param {string} type output file type - json / yml
 */
function renderSwaggerTemplate(so, templateFile, bindings, outputFile, type = 'json') {
  // read swagger defn. from file
  const jsonSwagger = JSON.parse(fs.readFileSync(templateFile, 'utf-8'));

  const apiGatewayName = constructApigwNameForServerless(so.userFnName);

  // transform swagger defn into aws apigateway resource
  const apigwResource = {
    Resources: {
      ApiGatewayRestApi: {
        Type: 'AWS::ApiGateway::RestApi',
        Properties: {
          Name: apiGatewayName,
          Description: `API for ${so.service}`,
          Body: jsonSwagger
        }
      }
    }
  };

  // apply the handle bar bindings
  const hbTemplate = hb.compile(JSON.stringify(apigwResource));
  let apigwResourceStr = hbTemplate(bindings);

  if (type !== 'json') {
    // transform to yml
    apigwResourceStr = YAML.stringify(JSON.parse(apigwResourceStr), 12, 2);
  }

  fs.writeFileSync(outputFile, apigwResourceStr);
  return apigwResourceStr;
}

/**
 * outputs the add permission resource for lambda in cloud formation template
 * form that defines the permission necessary for api gateway to invoke the
 * lambda function
 * @param {string} functionName lambda function name
 * @param {string} outputFile output filename for lambda permission resource
 * in cloud formation template in yml
 */
function renderPermissionTemplate(so, functionName, outputFile) {

  const slsLambdaName = constructLambdaNameForServerless(so.userFnName);
  const lambdaInstanceName = slsLambdaName + 'LambdaFunction';
  const lambdaPermissionResourceName = slsLambdaName + 'LambdaPermissionApiGateway';

  const lambdaPermission = {
    Resources: {
      [`${lambdaPermissionResourceName}`] : {
        Type: 'AWS::Lambda::Permission',
        DependsOn: [
          'ApiGatewayRestApi',
          lambdaInstanceName,
        ],
        Properties: {
          Action: 'lambda:InvokeFunction',
          FunctionName: functionName,
          Principal: 'apigateway.amazonaws.com'
        }
      }
    }
  };
  const lambdaPermissionYaml = YAML.stringify(lambdaPermission, 12, 2);
  fs.writeFileSync(outputFile, lambdaPermissionYaml);
}

/**
 * get the aws account number for the user where the resources will be deployed
 * uses the aws credentials file or environment variables
 */
const getAwsAccountNumber = async () => {
  const STS = require('aws-sdk/clients/sts');
  const sts = new STS({apiVersion: '2011-06-15'});
  const identity = await sts.getCallerIdentity().promise();
  return identity.Account;
};

getAwsAccountNumber().then(accountNo => {

  // Get the website URL to set it as the origin for requests for CORS
  const corsUrl = constructWebsiteUrl(soWebsite);

  // Construct the Lambda Uri to set it as endpoint for REST APIs
  const uri = constructLambdaUri(soFunction, accountNo);

  const functionName = soFunction.lambdaFnName;

  const bindings = {
    apiEndpointUri: uri,
    corsOriginList: corsUrl
  };

  console.log(`tufan.io: Generating ${apigwFilename} for serverless framework...`);
  renderSwaggerTemplate(soFunction, swaggerTemplate, bindings, apigwFilename, 'yml');

  console.log(`tufan.io: Generating ${permissionFilename} for serverless framework...`);
  renderPermissionTemplate(soFunction, functionName, permissionFilename);

  console.log('---');
});