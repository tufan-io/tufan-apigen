const merge = require('lodash.merge');
const fs = require('fs');

function handler (data, serverless, options) {
  const path = '../../../../mu-ui/build/appconfig.json';
  const uiAppConfig = require(path);
  const newAppConfig = merge({}, uiAppConfig, {
    authUrl: data.ServiceEndpoint
  });

  console.log(`writing to config file ${path}: ${JSON.stringify(newAppConfig, null, 2)}`);

  try {
    fs.writeFileSync(path, JSON.stringify(newAppConfig, null, 2));
  }
  catch(err) {
    throw new Error('Cannot write to file: ', path);
  }

  return Promise.resolve();
}

module.exports = { handler }
