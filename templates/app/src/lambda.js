'use strict'
const awsServerlessExpress = require('aws-serverless-express');
const app = require('./app');

/**
 * middleware to easily get the event object Lambda
 * receives from API Gateway
 *
 *     app.get('/', (req, res) => {
 *       res.json(req.apiGateway.event)
 *     })
 *
 */
const middleware = require('aws-serverless-express/middleware');
app.use(middleware.eventContext());


const server = awsServerlessExpress.createServer(app, () => {
  // call feathers app setup once server has started
  app.setup(server);
});


let isFeathersServerReady = false;

const EventEmitter = require('events');
class AppEmitter extends EventEmitter {
}
const emitter = new AppEmitter();


/**
 * Typically app.setup(server) calls sequelize.setup()
 * which means sequelize.setup() gets called after the
 * http server has started.
 *
 * Since sequelize setup is async, we want this to be
 * completed before we start processing incoming requests
 *
 * As sequelize sync does not depend on http server,
 * we can call setup during the lambda initialization
 *
 */
app.setup_sequelize(server)
  .then(() => {
    isFeathersServerReady = true;
    emitter.emit('initialized');
  });

const onInitialized = () => {
  return new Promise((resolve, reject) => {
    emitter.on('initialized', () => {
      console.log('initialized app');
      resolve();
    });
  });
};

const proxyEventToServer = (event, context) => {
  return new Promise(resolve => {
    awsServerlessExpress.proxy(server, event, {
      ...context,
      succeed: resolve
    });
  });
};

exports.handler = async (event, context) => {
  if (!isFeathersServerReady) {
    await onInitialized();
  }
  return proxyEventToServer(event, context);
}
