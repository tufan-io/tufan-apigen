const Sequelize = require('sequelize');

// ----------------------------------------------------------------------- //
// NOTE: tufan.io modification                                             //
// This is the only "enhancement" made by tufan.io to support ease of use. //
// It enables using `config/defaults.json` to use sqlite and mysql.        //
// This is done to ease getting started with fewer dependencies. Feel free //
// hack around this to suit your specific need.                            //
// ----------------------------------------------------------------------- //
function getSequelize(app) {
  const activeDb = ['sqlite', 'mysql', 'rds'].reduce((_, db) => {
    return _ || (app.get(db) ? db : null);
  }, (app.get('activedb') || null));
  console.log('active db configured: ', activeDb);
  switch (activeDb) {
    case 'mysql':
      {
        const connectionString = app.get('mysql');
        return new Sequelize(
          connectionString, {
            dialect: 'mysql',
            logging: false,
            operatorsAliases: false,
            define: {
              freezeTableName: true
            }
          });
      }
    case 'sqlite':
      {
        const connectionString = app.get('sqlite');
        return new Sequelize(
          connectionString, {
            dialect: 'sqlite',
            logging: false,
            operatorsAliases: false,
            define: {
              freezeTableName: true
            }
          });
      }
    case 'rds':
      {
        const dbconfig = {
          dbname: process.env.DBName,
          username: process.env.DBUsername,
          password: process.env.DBPassword,
          host: process.env.DBHost,
          port: process.env.DBPort,
          dialect: process.env.DBDialect
        };
        return new Sequelize(dbconfig.dbname, dbconfig.username, dbconfig.password, {
          host: dbconfig.host,
          port: dbconfig.port,
          dialect: dbconfig.dialect,
          operatorsAliases: false,
          timeout: 60000,
          define: {
            freezeTableName: true
          }
        });
      }
    default:
      const msg = [
        `Only sqlite and mysql are supported at this point.`,
        `You *could* hand-code the fix in '${__filename}', just saying...`
      ].join('\n');
      new Error(msg);
  }
}

module.exports = function (app) {
  const sequelize = getSequelize(app);
  app.set('sequelizeClient', sequelize);

  // sequelize (in general feathers service) setup is called as part of
  // feathers initialization giving the services a chance to set themselves
  // up once the server is started. feathers does not support setup
  // that is asynchronous in nature.
  // sequelize setup is async in nature and hence completing the
  // feathers setup does not guarantee that sequelize is in a state
  // ready to accept calls.
  // We explicitly separate out the setup of sequelize from the
  // feathers sequence and call sequelize setup separtely and await
  // on it till it is finished to relinquish control back
  app.setup_sequelize = async function (...args) {
    const result = '';

    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach(name => {
      if ('associate' in models[name]) {
        models[name].associate(models);
      }
    });

    // Sync to the database, enable logging sql statements for debugging
    await sequelize.sync({ logging: console.log });

    return result;
  };
};
