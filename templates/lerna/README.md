# {{ name.moduleName }}-service-repo

Tufan service is an opinionated micro-service mono-repo.

It's common for a web application to have a server and client


## Usage

Install all dependencies & start the api-server and ui-server in dev mode.
The ui-server is meant to serve static content eventually.
There is also a doc-server which serves static versions of a redoc swagger UI and a vanilla swagger-ui.
Both these will eventually be hooked up to work with an AWS lambda instance.

```bash
npm install
npm run start
```

`npm run start` starts both servers in parallel & ui-server takes control of
the console. Some times, it's necessary to have access to both - for debugging
and/or just for fun. Open two terminal windows and do one of the following in each.

1. `npm api-server:start`
2. `npm simple-ui:start`
3. `npm doc-server:start`

## Code Structure

```bash
├── README.md
├── package-lock.json
├── package.json
└── packages
    ├── api-server       # Backend [feathers.js based server](https://feathersjs.com/)
    ├── redoc            # [api documentation](https://rebilly.github.io/ReDoc/)
    ├── simple-ui        # Frontend [react based admin-on-rest](https://marmelab.com/admin-on-rest/)
    └── swagger          # [swagger-ui](https://github.com/swagger-api/swagger-ui)
```

`{{ name.moduleName }}-service-repo` gives you a quick starting point, for a best-practices
RESTful service. It builds upon popular community projects. Please refer to the
documentation for individual services to customize the repo to your needs.

We have attempted to make this a "batteries included" generation process, allowing
you to use the CLI tooling of underlying tool. We keep a proactive eye out for
breaking changes, but please report any troubles you encounter.

## RESTful conventions

There are no concrete recommendations on resource naming conventions for
RESTful APIs. So much so, that a microsoft API-guidelines explicitly
side-steps the issue - https://github.com/Microsoft/api-guidelines/issues/67 !

Since `{{ name.moduleName }}-service-repo`'s primary purpose is to generate APIs, it makes sense to
define a convention that we expect to follow.

A primary goal of RESTful APIs is to make it easy to consume and create data.
JSON is the de-facto standard for data consumption - especially on clients.

Hyphenated property access is more cumbersome in JSON/JavaScript.
`obj.CamelCaseProp => obj['hyphenated-prop']`

One necessarily wants to optimize for such client consumption.

However, file-systems are sometimes case-insensitive. In which case,
`CamelCaseProp.json` is the same as `camelcaseprop.json` or similar variants.

To that end, tufan-apigen adopts this convention

1. `files_names` are `snake_case`,
2. `objectAndPropertNames` are `camelCase`

One other piece to be resolved is whether API end-points should be singular or plural.
We choose singular. Primarily this allow us to use the resourceName in the UI naively

## Changelog

`{{ name.moduleName }}-service-repo` is a mono-repo, without dependencies, and
therefore, does not at this point come bundled with lerna or yarn-workspaces.
It is however structured to be lerna/yarn-workspace compatible, should your
application grow to need it.

[Changelog](./CHANGELOG.md)

## License

[Apache-2.0](./LICENSE.md)

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](code-of-conduct.md). By participating in this project you agree to abide by its terms.

## Support

Bugs, PRs, comments, suggestions welcomed!
