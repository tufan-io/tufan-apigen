// #!/usr/bin/env node

// const program = require('commander');
// const chalk = require('chalk');
// const renderDir = require('render-dir').renderDir;
// const pkg = require('./package.json');

// function bundleClients(option_c, pkgMap, promises) {
// 	option_c = option_c.trim().split(',');
// 	const clients = new Set(option_c.filter(c => c !== 'server'));
// 	for (let c of clients) {
// 		if (!(c in pkgMap)) {
// 			console.error(chalk.red(`'${c}' is an unsupported client`));
// 		}
// 		// using render-dir as (mkdirp + deep-copy)
// 		promises.push(
// 			renderDir(
// 				pkgMap[c].src,
// 				`dist/${pkgMap[c].dst}`
// 			));
// 	}
// 	return promises;
// }

// function bundleServer(option_s, pkgMap, promises) {
// 	option_s = option_s.trim().split(',');
// 	const serverComponents = new Set(['server'].concat(option_s));
// 	for (let s of serverComponents) {
// 		if (!(s in pkgMap)) {
// 			console.error(chalk.red(`'${s}' is an unsupported client`));
// 		}
// 		// using render-dir as (mkdirp + deep-copy)
// 		promises.push(
// 			renderDir(
// 				pkgMap[s].src,
// 				`dist/server/${pkgMap[s].dst}`
// 			));
// 	}
// 	return promises;
// }

// function bundle(program) {
// 	const pkgMap = {
// 		'simple-ui': {
// 			src: `./packages/front-end/simple-ui/dist`,
// 			dst: `public/admin`
// 		},
// 		swagger: {
// 			src: `./packages/swagger`,
// 			dst: `public/swagger`
// 		},
// 		redoc: {
// 			src: `./packages/redoc`,
// 			dst: `public/redoc`
// 		},
// 		server: {
// 			src: `./packages/server`,
// 			dst: `.`
// 		}
// 	}
//   let promises = [];
//   console.log(program)
// 	promises = bundleClients(program.client, pkgMap, promises);
// 	promises = bundleServer(program.server, pkgMap, promises);
// 	return Promise.all(promises)
// 		.catch((err) => {
//       console.log(chalk.red(err.message));
// 		});
// }

// program
// 	.version(pkg.version)
// 	.description([
// 		`Bundles the client and server components.`,
// 		`By default, client & server include all clients, to make getting started easier.`,
// 		`In production, customize to suit your taste`
// 	].join('\n'))
// 	.option(
// 		`-c, --client <clients>`,
// 		`comma seperated list of components (select from - simple-ui,swagger,redoc)`,
// 		`simple-ui,swagger,redoc`)
// 	.option(
// 		`-s, --server <server>`,
// 		`comma seperated list of components (select from - simple-ui,swagger,redoc)`,
// 		`simple-ui,swagger,redoc`
// 	)
// 	.parse(process.argv)

// bundle(program);
