import { OnInit } from '@angular/core';
import { BaseComponent, SpecManager } from '../base';
export declare class ApiLogo extends BaseComponent implements OnInit {
    logo: any;
    constructor(specMgr: SpecManager);
    init(): void;
    ngOnInit(): void;
}
