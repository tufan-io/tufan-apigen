# template caches

tufan-apigen relies on multiple other npm modules to provide various functionality.
Specifically
- `generator-feathers` is a yeoman generator for a featherjs app.
- `swagger-ui-dist` provides a swagger ui
