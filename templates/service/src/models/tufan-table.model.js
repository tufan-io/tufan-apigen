// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

{{~info "  > TABLE %s" name.singular.camel ~}}
module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const model = sequelizeClient.define('{{ name.singular.camel }}', {
    {{~#each fields~}}
    {{~info "    + FIELD %s %s" name.singular.camel type}}
    {{ name.singular.camel }}: {
      {{#if length}}
      length: {{length}},
      {{/if}}
      {{~#if options}}
      options: {
        length: {{options.length}}
      },
      {{/if}}
      {{~#if allowNull}}
      allowNull: {{allowNull}},
      {{/if}}
      {{~#if unique}}
      unique: {{unique}},
      {{/if}}
      {{~#if defaultValue}}
      defaultValue: {{defaultValue}},
      {{/if}}
      {{~#if primaryKey}}
      primaryKey: '{{primaryKey}}',
      {{/if}}
      {{~#if field}}
      field: '{{field}}',
      {{/if}}
      {{~#if autoIncrement}}
      autoIncrement: {{autoIncrement}},
      {{/if}}
      {{~#if comment}}
      comment: '{{comment}}',
      {{/if}}
      type: DataTypes.{{uppercase type}}
    }{{#unless @last}},{{/unless}}
    {{/each}}
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  model.associate = function (models) { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    {{#each associations}}
    {{~info "  <> %s :%s: %s" source type target}}
    models.{{ source }}.{{ type }}(models.{{ target }}, {{{stringify options }}});
    {{/each}}
  };

  return model;
};
