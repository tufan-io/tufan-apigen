// Initializes the `{{ name.propObj }}` service on path `/{{ name.propObj }}`
const createService = require('feathers-sequelize');
const createModel = require('../../models/{{ name.singular.kebab }}.model');
const hooks = require('./{{ name.singular.kebab }}.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: '{{ name.propObj }}',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/{{ name.propObj }}', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('{{ name.propObj }}');

  service.hooks(hooks);
};
