const assert = require('assert');
const app = require('../../src/app');

describe('\'{{ name.propObj }}\' service', () => {
  it('registered the service', () => {
    const service = app.service('{{ name.propObj }}');

    assert.ok(service, 'Registered the service');
  });
});
