<a name="1.0.0"></a>
# 1.0.0 (2017-11-21)


### Bug Fixes

* adding config diretory to package files ([36bbbed](https://bitbucket.org/tufan-io/tufan-apigen/commits/36bbbed))
* Adding template directories to package files ([a85dbde](https://bitbucket.org/tufan-io/tufan-apigen/commits/a85dbde))
* committing swagger-ui-dist/package.json in updated form. ([14f9174](https://bitbucket.org/tufan-io/tufan-apigen/commits/14f9174))
* do not copy package.json and .npmignore into cache. Makes the cache based significant change de ([75b081c](https://bitbucket.org/tufan-io/tufan-apigen/commits/75b081c))
* Fixes automated dependency update detector to actually fail ([a0691f9](https://bitbucket.org/tufan-io/tufan-apigen/commits/a0691f9))
* Fixing the success case for the update test ([ac91345](https://bitbucket.org/tufan-io/tufan-apigen/commits/ac91345))
* include schema.json in package files ([a5664fa](https://bitbucket.org/tufan-io/tufan-apigen/commits/a5664fa))
* moving dependency from data-ok to data-check, since the module has been renamed ([dfda0ef](https://bitbucket.org/tufan-io/tufan-apigen/commits/dfda0ef))
* update dist files for automated significant dependency change detection ([c749275](https://bitbucket.org/tufan-io/tufan-apigen/commits/c749275))
* Upgrading template dependencies ([a0fc527](https://bitbucket.org/tufan-io/tufan-apigen/commits/a0fc527))
* uses *Fields instead of *Input on list elements and other minor fixes, so upgraded admin-on-rest UI works ([f207653](https://bitbucket.org/tufan-io/tufan-apigen/commits/f207653))
* WIP: committing mostly working implementtion - still having trouble communicated with the backe ([b0f06db](https://bitbucket.org/tufan-io/tufan-apigen/commits/b0f06db))


### Features

* Adding a test to automatically detect significant updates to template dependencies. ([790b30d](https://bitbucket.org/tufan-io/tufan-apigen/commits/790b30d))
* Adding support for associations in the data specification of the model ; ([ff0d541](https://bitbucket.org/tufan-io/tufan-apigen/commits/ff0d541))
* Big Bang: Initial commit ([dd4de8c](https://bitbucket.org/tufan-io/tufan-apigen/commits/dd4de8c))
* Refined ability to generated consistent name - api endpt, obj/prop name, UI component ([42d93ef](https://bitbucket.org/tufan-io/tufan-apigen/commits/42d93ef))



